library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

USE WORK.mytypes.ALL;

-- @template:
--



entity afe2ram is
    generic(
        RAM_ADDR_WIDTH : integer := 12;
        RAM_DATA_WIDTH : integer := 8);
    port(   
        clk             : in  std_logic; 
        afe_en_i        : in  std_logic;
        afe_signal_i    : in  unarray_plus((N+1) DOWNTO 1);   
        ram_wr_en       : out std_logic;   
        ram_wr_addr_o   : out std_logic_vector(ADDR_WIDTH-1 downto 0);    
        ram_wr_data_o   : out std_logic_vector(DATA_WIDTH-1 downto 0);  
        busy_o          : out std_logic
    );
end afe2ram;

architecture Behavioral of afe2ram is

    signal new_data_flag_tick : std_logic;

begin
    -- falling edge detector for afe_en_i
    -- here is when the data is ready to read
    process(clk)
    begin
        if(rising_edge(clk)) then
            q <= afe_en_i;
        end if;
    end process;
    new_data_flag_tick <= (not afe_en_i) and q;

    
            
end Behavioral;