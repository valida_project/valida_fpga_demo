# FPGA VALIDA
Proyecto VHDL para implementación DEMO para JL, en FPGA DE10-nano. 

Ver [aquí](_doc/DEMO%20VALIDA_OUT_CSV.pdf) para más info. 

### CARPETAS
- `afe`: se encuentran todos los ficheros fuentes del diseño de JL. 
- `DE10nano`: ficheros necesarios para construir el proyecto en Quartus. 
- `ram`: ficheros vhdl en caso de necesitar la RAM. 
- `uart`: ficheros vhdl para el manejo de la UART. 

# DEFINICIÓN DE FICHERO TOP

### Entity: main 
- **File**: main.vhd

![Diagram](_doc/main.svg "Diagram")
### Generics

| Generic name           | Type    | Value | Description |
| ---------------------- | ------- | ----- | ----------- |
| AUDIO_GAIN             | integer | 4     |             |
| AUDIO_OFFSET           | integer | -7117 |             |
| AUDIO_THRESH           | integer | 150   |             |
| AUDIO_RESOLUTION_I     | integer | 6     |             |
| VALIDA_EXP_DELTA       | integer | 1     |             |
| VALIDA_MAC_THRESH_HIGH | integer | 4000  |             |
| VALIDA_MAC_THRESH_LOW  | integer | 20000 |             |

### Ports

| Port name | Direction | Type                         | Description |
| --------- | --------- | ---------------------------- | ----------- |
| clk       | in        | std_logic                    |             |
| rst       | in        | std_logic                    |             |
| o_i2s_clk | out       | std_logic                    |             |
| i_i2s_din | in        | std_logic                    |             |
| o_i2s_ws  | out       | std_logic                    |             |
| i_uart_rx | in        | std_logic                    |             |
| o_uart_tx | out       | std_logic                    |             |
| i_train   | in        | std_logic                    |             |
| o_led     | out       | std_logic_vector(7 downto 0) |             |