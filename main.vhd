library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
USE WORK.mytypes.ALL; --Auxiliar file for configuration purporses
--------------------------------------------------------------------------
-- @Info:
-- --------------------
-- 
--------------------------------------------------------------------------
-- @History:
-- Fecha		VERSION		Comments
-- --------------------------------------------------
-- 
-----------------------------------------------------
entity main is 
    generic(
        AUDIO_GAIN              : integer := 4;
        AUDIO_OFFSET            : integer := -7117;
        AUDIO_THRESH            : integer := 150;
        AUDIO_RESOLUTION_I      : integer := 6;
        VALIDA_EXP_DELTA        : integer := 1;
        VALIDA_MAC_THRESH_HIGH  : integer := 4000;
        VALIDA_MAC_THRESH_LOW   : integer := 20000
    );
    Port ( 	clk             : in std_logic;		
            rst             : in std_logic;
            -- Pines i2s
            o_i2s_clk       : out std_logic;
            i_i2s_din       : in  std_logic;
            o_i2s_ws        : out std_logic;
            --Pines de uart
            i_uart_rx       : in  std_logic;
            o_uart_tx       : out std_logic;
            -- push button to train
            i_train         : in  std_logic;
            -- led
            o_led           : out std_logic_vector(7 downto 0)			
    );
end main;

architecture rtl of main is
    constant RAM_EXT_ADD_WIDTH      : integer := 12;
    constant RAM_EXT_DATA_WIDTH     : integer := 8;

    constant AFE_INPUT_MIN_VALUE : signed := to_signed(-2**(RC_PRECISION-1), RC_PRECISION);
    constant AFE_INPUT_MAX_VALUE : signed := to_signed(2**(RC_PRECISION-1)-1, RC_PRECISION);
     
     signal rstn : std_logic;
    
    signal fsm_led                  :  std_logic_vector(2 downto 0);
    signal fsm_audioRAM_wr_addr_i 	:  std_logic_vector(RAM_EXT_ADD_WIDTH-1 downto 0);
    signal fsm_audioRAM_wr_data_i 	:  std_logic_vector(RAM_EXT_DATA_WIDTH-1 downto 0);  
    signal fsm_audioRAM_wr_en_i 	:  std_logic;
    signal fsm_audioRAM_rd_addr_i 	:  std_logic_vector(RAM_EXT_ADD_WIDTH-1 downto 0);
    signal fsm_audioRAM_rd_data_o 	:  std_logic_vector(RAM_EXT_DATA_WIDTH-1 downto 0);  
    signal fsm_audioRAM_rd_en_i 	:  std_logic;
    signal fsm_afe_input_control 	:  std_logic;
    signal fsm_afe_i_enable 	    :  std_logic;
    signal fsm_afe_i_senyal_u 	    :  signed ((rc_precision -1) downto 0);
    
    -- audio i2s
    signal audio_i_gain         : std_logic_vector(4 downto 0);
    signal audio_i_offset       : std_logic_vector(15 downto 0);
    signal audio_i_level_th     : std_logic_vector(15 downto 0);
    signal audio_o_active       : std_logic;
    signal audio_o_en		    : std_logic;
    signal audio_o_data		    : std_logic_vector(15 downto 0);
    signal audio_resolution     : std_logic_vector(3 downto 0);
    signal audio_resolution_int : integer range 0 to 15;
    signal audio_data_cropped   : signed(7 downto 0);

    -- AFE
    -- Signals to control AFE input
    CONSTANT AFE_INPUT_CONTROL_MICRO : std_logic := '0';
    CONSTANT AFE_INPUT_CONTROL_FSM   : std_logic := '1';
    signal valida_i_nreset : std_logic;
    signal valida_i_enable : std_logic;
    signal valida_i_senyal_u : signed ((rc_precision -1) downto 0) := (others=>'0');
    signal afe_o_out_enable : std_logic;
    signal afe_o_weights : unarray_plus ((n+1) downto 1) := (others => (others=>'0'));
    signal valida_o_y_out : std_logic;
    signal valida_o_detect : std_logic;
    signal valida_o_state_bit : std_logic;
    signal valida_o_reservoir : signed(2*BIT_PRECISION - 1 DOWNTO 0);
    signal valida_o_internal_enable : std_logic;

    signal valida_i_exp_delta : INTEGER RANGE 0 TO 3;
    signal valida_i_mac_thresh_high : INTEGER RANGE 0 TO 262143;
    signal valida_i_mac_thresh_low : INTEGER RANGE 0 TO 262143;
  
    -- Component declaration for i2sm
    component i2sm_in is
    generic(
        DIV : integer := 98        
    );
    port(
        clk : in std_logic;
        resetn : in std_logic;
        o_ld : out std_logic_vector(15 downto 0);
        o_rd : out std_logic_vector(15 downto 0);
        o_smp_we : out std_logic;
        i_dc_value : in std_logic_vector(15 downto 0);
        i_level_th : in std_logic_vector(15 downto 0);
        o_active : out std_logic;
        o_ld_with_dc : out std_logic_vector(15 downto 0);
        i_gain : in std_logic_vector(4 downto 0);
        o_bclk : out std_logic;
        i_sd : in std_logic;
        o_ws : out std_logic        
    );
    end component;

begin

    -----------------------------------
    -- MIC driver I2S
    -----------------------------------
    I2S_mic_inst : i2sm_in
    generic map(DIV=>98)
    port map (	
        clk,
        rstn,
        open,
        open,
        audio_o_en,
        audio_i_offset,
        audio_i_level_th,
        audio_o_active, 	
        audio_o_data,
        audio_i_gain,
        o_i2s_clk, 	
        i_i2s_din,	
        o_i2s_ws	
    );
     
    audio_i_gain                <= std_logic_vector(to_unsigned(AUDIO_GAIN, audio_i_gain'length));
    audio_i_offset              <= std_logic_vector(to_signed  (AUDIO_OFFSET, audio_i_offset'length));
    audio_i_level_th            <= std_logic_vector(to_unsigned(AUDIO_THRESH, audio_i_level_th'length));
    audio_resolution            <= std_logic_vector(to_unsigned(AUDIO_RESOLUTION_I, audio_resolution'length));
    valida_i_exp_delta          <= VALIDA_EXP_DELTA;
    valida_i_mac_thresh_high    <= VALIDA_MAC_THRESH_HIGH;
    valida_i_mac_thresh_low     <= VALIDA_MAC_THRESH_LOW;
    -----------------------------------
    -- FSM communication UART Layer
    -----------------------------------
    CommLayer_inst : entity work.CommLayer 
    generic map(RAM_EXT_ADD_WIDTH   => RAM_EXT_ADD_WIDTH, 
                RAM_EXT_DATA_WIDTH  => RAM_EXT_DATA_WIDTH)   
    port map (  
        clk, 
        rst, 
        fsm_led, 
        i_uart_rx, 
        o_uart_tx,
        -- RAM
        fsm_audioRAM_wr_addr_i, fsm_audioRAM_wr_data_i, fsm_audioRAM_wr_en_i,
        fsm_audioRAM_rd_addr_i, fsm_audioRAM_rd_data_o, fsm_audioRAM_rd_en_i,
        -- Audio i2s
        audio_o_en, 
        audio_o_data,
        open, -- audio_i_gain,
        open, -- audio_i_offset,
        open, -- audio_i_level_th,
        open, -- audio_resolution,
        -- cropped audio
        audio_data_cropped,
        -- afe
        fsm_afe_input_control,
        valida_i_nreset,
        fsm_afe_i_enable,
        fsm_afe_i_senyal_u,
        afe_o_out_enable,
        afe_o_weights,
        open, -- valida_i_exp_delta,
        open, -- valida_i_mac_thresh_high,
        open,  -- valida_i_mac_thresh_low
        valida_o_detect,
        valida_o_state_bit,
        valida_o_y_out,
        valida_o_reservoir,
        valida_o_internal_enable
        );

    -----------------------------------
    -- SRAM External
    -----------------------------------
    spRam_inst : entity work.single_port_ram
    generic map(RAM_EXT_ADD_WIDTH, RAM_EXT_DATA_WIDTH)
    port map (
        clk, 
        fsm_audioRAM_wr_en_i,
        fsm_audioRAM_wr_addr_i,
        fsm_audioRAM_wr_data_i,
        fsm_audioRAM_rd_en_i,
        fsm_audioRAM_rd_addr_i,
        fsm_audioRAM_rd_data_o
    );

    -----------------------------------
    -- VALIDA
    -----------------------------------
    inst_afe : entity work.valida
    port map (  clk                 => clk,
                ctrl_n_rst          => rstn,
                ctrl_en             => valida_i_enable,
                i_train             => i_train,
                i_senyal_u          => valida_i_senyal_u,
                i_exp_delta         => valida_i_exp_delta,
                i_mac_thresh_high   => valida_i_mac_thresh_high,
                i_mac_thresh_low    => valida_i_mac_thresh_low,
                o_reservoir         => valida_o_reservoir,
                o_y_out             => valida_o_y_out,
                o_internal_enable   => valida_o_internal_enable,
                o_state_bit         => valida_o_state_bit,
                o_detect            => valida_o_detect                
    );



    -- MUXES to control de enable and senyal input
    -- These inputs can be handled by the FSM or the i2s_microphone
    -- 'fsm_afe_input_control' decides when.
    valida_i_enable <= audio_o_en when fsm_afe_input_control = AFE_INPUT_CONTROL_MICRO else
                    fsm_afe_i_enable;
    valida_i_senyal_u <= audio_data_cropped when fsm_afe_input_control = AFE_INPUT_CONTROL_MICRO else
                      fsm_afe_i_senyal_u;

    -- TODO: Saturar las salidas
    audio_resolution_int    <= to_integer(unsigned(audio_resolution));
    process(clk, audio_resolution_int, audio_o_data)
    begin
        if rising_edge(clk) then
            case audio_resolution_int is
                when 0 =>
                    audio_data_cropped  <= signed(audio_o_data(7 downto 0));
                    if    (signed(audio_o_data) < -2**7) then
                           audio_data_cropped  <= AFE_INPUT_MIN_VALUE;
                    elsif (signed(audio_o_data) > (2**7-1)) then
                           audio_data_cropped  <= AFE_INPUT_MAX_VALUE;
                    end if;
                when 1 =>
                    audio_data_cropped  <= signed(audio_o_data(8 downto 1));
                    if    (signed(audio_o_data) < -2**8) then
                            audio_data_cropped  <= AFE_INPUT_MIN_VALUE;
                    elsif (signed(audio_o_data) > (2**8-1)) then
                            audio_data_cropped  <= AFE_INPUT_MAX_VALUE;
                    end if;
                when 2 =>
                    audio_data_cropped  <= signed(audio_o_data(9 downto 2));
                    if    (signed(audio_o_data) < -2**9) then
                            audio_data_cropped  <= AFE_INPUT_MIN_VALUE;
                    elsif (signed(audio_o_data) > (2**9-1)) then
                            audio_data_cropped  <= AFE_INPUT_MAX_VALUE;
                    end if;
                when 3 =>
                    audio_data_cropped  <= signed(audio_o_data(10 downto 3));
                    if    (signed(audio_o_data) < -2**10) then
                            audio_data_cropped  <= AFE_INPUT_MIN_VALUE;
                    elsif (signed(audio_o_data) > (2**10-1)) then
                            audio_data_cropped  <= AFE_INPUT_MAX_VALUE;
                    end if;
                when 4 =>
                    audio_data_cropped  <= signed(audio_o_data(11 downto 4));
                    if    (signed(audio_o_data) < -2**11) then
                            audio_data_cropped  <= AFE_INPUT_MIN_VALUE;
                    elsif (signed(audio_o_data) > (2**11-1)) then
                            audio_data_cropped  <= AFE_INPUT_MAX_VALUE;
                    end if;
                when 5 =>
                    audio_data_cropped  <= signed(audio_o_data(12 downto 5));
                    if    (signed(audio_o_data) < -2**12) then
                            audio_data_cropped  <= AFE_INPUT_MIN_VALUE;
                    elsif (signed(audio_o_data) > (2**12-1)) then
                            audio_data_cropped  <= AFE_INPUT_MAX_VALUE;
                    end if;
                when 6 =>
                    audio_data_cropped  <= signed(audio_o_data(13 downto 6));
                    if    (signed(audio_o_data) < -2**13) then
                            audio_data_cropped  <= AFE_INPUT_MIN_VALUE;
                    elsif (signed(audio_o_data) > (2**13-1)) then
                            audio_data_cropped  <=  AFE_INPUT_MAX_VALUE;
                    end if;
                when 7 =>
                    audio_data_cropped  <= signed(audio_o_data(14 downto 7));
                    if    (signed(audio_o_data) < -2**14) then
                            audio_data_cropped  <= AFE_INPUT_MIN_VALUE;
                    elsif (signed(audio_o_data) > (2**14-1)) then
                            audio_data_cropped  <=  AFE_INPUT_MAX_VALUE;
                    end if;
                when others =>
                    audio_data_cropped  <= signed(audio_o_data(15 downto 8));
                    if    (signed(audio_o_data) < -2**15) then
                            audio_data_cropped  <= AFE_INPUT_MIN_VALUE;
                    elsif (signed(audio_o_data) > (2**15-1)) then
                            audio_data_cropped  <=  AFE_INPUT_MAX_VALUE;
                    end if;
            end case;            
        end if;
    end process;
    
    rstn <= not rst;
    -- output connection
    o_led <= i_train            &
             valida_o_state_bit & 
             valida_o_detect    &
             "000"              & 
             valida_o_y_out     & 
             audio_o_active;

end rtl;

