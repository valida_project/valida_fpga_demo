library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- @template:
--
-- constant ADDR_WIDTH: integer := 12;
-- constant DATA_WIDTH: integer := 8;

-- signal wr_en_i     :   std_logic;   
-- signal wr_addr_i   :   std_logic_vector(ADDR_WIDTH-1 downto 0);    
-- signal wr_data_i   :   std_logic_vector(DATA_WIDTH-1 downto 0);  
-- signal rd_en_i     :   std_logic;   
-- signal rd_addr_i   :   std_logic_vector(ADDR_WIDTH-1 downto 0);    
-- signal rd_data_o   :   std_logic_vector(DATA_WIDTH-1 downto 0); 


-- inst_ram : single_port_ram 
-- generic map(
--     ADDR_WIDTH, DATA_WIDTH
-- )
-- port map(
--     clk, wr_en_i, wr_addr_i, wr_data_i, rd_en_i,
--     rd_addr_i, rd_data_o
-- );


entity single_port_ram is
    generic(
        ADDR_WIDTH : integer := 12;
        DATA_WIDTH : integer := 8);
    port(   
        clk         : in  std_logic; 
        wr_en_i     : in  std_logic;   
        wr_addr_i   : in  std_logic_vector(ADDR_WIDTH-1 downto 0);    
        wr_data_i   : in  std_logic_vector(DATA_WIDTH-1 downto 0);  
        rd_en_i     : in  std_logic;   
        rd_addr_i   : in  std_logic_vector(ADDR_WIDTH-1 downto 0);    
        rd_data_o   : out std_logic_vector(DATA_WIDTH-1 downto 0)  
    );
end single_port_ram;

architecture Behavioral of single_port_ram is

    signal reg_data_o : std_logic_vector(DATA_WIDTH-1 downto 0);
    --type and signal declaration for RAM.
    type ram_type is array(0 to 2**ADDR_WIDTH-1) of std_logic_vector(DATA_WIDTH-1 downto 0);
    signal ram : ram_type := (others => (others => '0'));

begin

    process(clk)
    begin
        if(rising_edge(clk)) then
            if(wr_en_i = '1') then    
                ram(conv_integer(wr_addr_i)) <= wr_data_i;
            end if;
            if(rd_en_i = '1') then    
                reg_data_o <= ram(conv_integer(rd_addr_i));
            end if;
        end if;
    end process;

    rd_data_o <= reg_data_o;
            
end Behavioral;