----------------------------------------------------------------------------------
-- Fecha : 13/03/2016
-- Datos personales : Christiam Franco, Y1475493B
-- Codigo de test para MSEEI 2016, FPGA
-- UART Tx
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;
use work.Tx_UART_pack.all;

----------------------------------------------------------------------------------
----Template de iniciacion
----Iniciacion de txUart
--    Tx_UART : entity work.uart_tx  
-- generic map(
--         DBIT=>8,     -- # data bits, por defecto 8 bits de transmision
-- 	    TICKS_P_BIT=>TICKS_P_BIT,     -- # ticks por bit
--         SB_TICK=>TICKS_P_BIT*2 -- # ticks for stop bits, normalmente deberia ser TICKS_P_BIT, pero en alta velocidad, el tiempo de stop es mayor
--     )
--    port map(
--      clk=>clk,--reloj
--      reset=>reset,--reset async
--      tx=>tx,--pin de transmision
--      s_tick=>br_tick,--Conectar a generador de Baudrate
--      tx_start_tick => tx_start_tick ,--Strobe para iniciar la transmision
--      tx_done_tick => tx_done_tick,--Strobe para inidicar que termino la transmision
--      din=>din--Dato a ser transmitido
--    );
-------------------------------------------------------------------------------------

entity uart_tx is
generic(
      DBIT: integer:=8;     -- # data bits, por defecto 8 bits de transmision
		TICKS_P_BIT : integer:=6;     -- # ticks por bit
      SB_TICK: integer:=6*2 -- # ticks for stop bits, normalmente deberia ser TICKS_P_BIT, pero en alta velocidad, el tiempo de stop es mayor
   );
   port(
      clk, reset: in std_logic;
      txUART_i : in TxUART_iType;
      txUART_o : out TxUART_oType     
   );
end uart_tx;

architecture Behavioral of uart_tx is

   type state_type is (idle, NextData,start, data, stop);
   type reg_type is record
    state : state_type;
    s : unsigned(5 downto 0);
    n : unsigned(2 downto 0);
    b : std_logic_vector(7 downto 0);
    tx : std_logic;
   end record;
   signal reg,rNext: reg_type;
   
begin

-- FSMD Se�ales registradas
   process(clk)
   begin
      if (clk'event and clk='1') then
         reg <= rNext;         
      end if;
   end process;
   -- next-state logic & data path functional units/routing
   process(reg, txUART_i)
   begin
      --Default state
      rNext <= reg;      
      txUART_o.tx_done_tick<='0';
      case reg.state is
         when idle =>   
            rNext.tx <= '1'; --tx a '1'        
            if txUART_i.tx_start_tick='1' then--Si debo iniciar ahora
               rNext.state <= NextData;--next state
               rNext.s <= (others=>'0');--reset contador 
					txUART_o.tx_done_tick<='1';--Habilita siguiente dato en FIFO                       
            end if;
			when NextData =>
				rNext.b <= txUART_i.din;--capturo el dato a enviar   
				rNext.state <= start;--next state
         when start =>
            rNext.tx <= '0';--bit de start '0'
            if (txUART_i.s_tick = '1') then
               if reg.s=(TICKS_P_BIT-1) then--espera x pulsos de baudgen
                  rNext.state <= data;
                  rNext.s <= (others=>'0');--reset contador
                  rNext.n <= (others=>'0');--inicio cuenta de bits enviados
               else
                  rNext.s<= reg.s + 1;--incremento contador de baud
               end if;
            end if;
         when data =>--Estado de transmision de byte
            rNext.tx <= reg.b(0);--bit LSB out
            if (txUART_i.s_tick = '1') then
               if reg.s=(TICKS_P_BIT-1) then--espera x pulsos
                  rNext.s <= (others=>'0');--reset cont
                  rNext.b <= '0' & reg.b(7 downto 1) ;--b>>1                  
                  if reg.n=(DBIT-1) then-- si ya termino de enviar todos los bits
                     rNext.state <= stop ;--envio bit de stop                                     
                  else
                     rNext.n <= reg.n + 1;--incremento num de bits enviados
                  end if;
               else
                  rNext.s <= reg.s + 1;--incremento contador de baud
               end if;
            end if;
         when stop =>
            rNext.tx <= '1';--bit de stop '1'
            if (txUART_i.s_tick = '1') then               
               if reg.s=(SB_TICK-1) then--espera el numero de tick de bit de stop
                  rNext.state <= idle;
                  --txUART_o.tx_done_tick<='1';--avisa que se ha enviado el byte y esta listo para enviar otro
               else
                  rNext.s <= reg.s + 1;
               end if;
            end if;
      end case;
   end process;
   --Salida de UART
   txUART_o.tx <= reg.tx;
   
end Behavioral;
