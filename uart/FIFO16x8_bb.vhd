library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity FIFO16x8_bb is

	port (    clk : in std_logic;
				 reset : in std_logic;
				 enr : in std_logic;   --enable read,should be '0' when not in use.
				 enw : in std_logic;    --enable write,should be '0' when not in use.
				 dataout : out std_logic_vector(7 downto 0);    --output data
				 datain : in std_logic_vector (7 downto 0);     --input data
				 empty : out std_logic;     --set as '1' when the queue is empty
				 full : out std_logic     --set as '1' when the queue is full
				);
end FIFO16x8_bb;

architecture Behavioral of FIFO16x8_bb is
	type memory_type is array (0 to 15) of std_logic_vector(7 downto 0);
	signal memory : memory_type :=(others => (others => '0'));   --memory for queue.
	signal readptr,writeptr : std_logic_vector(3 downto 0) :=(others=>'0');  --read and write pointers.
	signal dout_s : std_logic_vector(7 downto 0);
begin
	process(clk, reset, enr, enw)
	begin
		if(reset='1') then
			readptr <= (others=>'0');
			writeptr <= (others=>'0');			
		else
			if(clk'event and clk='1' and enr ='1') then
				dout_s <= memory(conv_integer(readptr));
				readptr <= readptr + '1';      --points to next address.
			end if;
			if(clk'event and clk='1' and enw ='1') then
				memory(conv_integer(writeptr)) <= datain;
				writeptr <= writeptr + '1';  --points to next address.
			end if;	
		end if;
	end process;
	
	
	dataout <= dout_s;
	
	-- Señales de aviso externas
	full <=  '1' when (writeptr-readptr)="1111" else
				'0';
	empty <= '1' when (writeptr = readptr) else
				'0';
	

end Behavioral;
