----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 08.04.2016 23:46:00
-- Design Name: 
-- Module Name: UART_pack - 
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


package UART_pack is
    type UART_iType is record
        rxPin :STD_LOGIC;
        readStrobe : STD_LOGIC;
        TxData : STD_LOGIC_VECTOR (7 downto 0);
        TxStrobe : STD_LOGIC;
    end record;
    type UART_oType is record
        TxPin :  STD_LOGIC;
        RxData :  STD_LOGIC_VECTOR (7 downto 0);
        RxEmpty : STD_LOGIC;
        TxFull : STD_LOGIC;
    end record;
end UART_pack;
