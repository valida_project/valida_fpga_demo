
--library IEEE;
--use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

library IEEE;
use IEEE.STD_LOGIC_1164.all;

package Rx_UART_pack is
    type RxUart_iType is record
        rx : std_logic;
        s_tick: std_logic;        
    end record;
    type RxUart_oType is record
        rx_done_tick: std_logic;
        dout: std_logic_vector(7 downto 0);      
    end record;
end Rx_UART_pack;
