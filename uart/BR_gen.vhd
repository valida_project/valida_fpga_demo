----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 11.03.2016 14:50:07
-- Design Name: 
-- Module Name: BR_gen - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity BR_gen is
    ---------------------------------------------------
    -- generador de baudrate para modulo de UART
    -- N=fclk/(16*Baudrate)
    -- para clk=100MHz
    -- -----------------------
    -- Baudrate     N
    -- -----------------------
    --    19200    326
    --     9600    651
    -----------------------------------------------------
    generic(
        N : natural := 326
    );
    Port ( clk : in STD_LOGIC;
           br_tick : out STD_LOGIC);
end BR_gen;

architecture Behavioral of BR_gen is

    signal contr,contn : NATURAL range 0 to N-1;

begin
    --se�al registrada
    process(clk)
    begin
        if rising_edge(clk) then
            contr <= contn;
        end if;
    end process;
    --estado siguiente se borra al llegar al limite
    contn<=0 when contr=N-1 else contr+1;
    --br_tick solo se pone a '1' durante 1 ciclo de reloj
    br_tick<='1' when contr=N-1 else '0';

end Behavioral;
