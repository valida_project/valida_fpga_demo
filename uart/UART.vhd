library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;
use work.UART_pack.all;
use work.Rx_UART_pack.all;
use work.Tx_UART_pack.all;


--
--
-- notas para calcular el baudrate y saber si sera preciso:
--  ver el excel que calcula esto... 
--          CalculoErrorUART_FPGA.xlsx
--
-- TEMPLATE :
-- Se debe declarar el package de UART:
-- use work.UART_pack.all;
--
-- -- Numero de ciclos de retardo que equivalen a 1 byte en UART
-- 	constant BAUDRATE : natural :=115200;--baudrate real en bps
-- 	constant CLK_FREQ : natural :=3072000;--reloj de sistema en Hz
-- 	constant TICKS_P_BIT : natural := 3;
-- 	constant SB_TICK : natural := TICKS_P_BIT;--Stop bit ticks, for high speed = TICKS_P_BIT*2
--  constant N : natural := 9;--Round this parameter from excel sheet  (CLK_frec/(TICKS_P_BIT * BAUDRATE))
	
-- --      --Se�ales de UART
--      signal UART_i : UART_iType;
--      signal UART_o : UART_oType;
--
--    --Instantiation
-- --DEclaracion para modulo UART
-- 		uart_inst : entity work.UART
-- 		generic map(BAUDRATE , CLK_FREQ, TICKS_P_BIT, SB_TICK, N)
-- 		port map(clk, '0', UART_i, UART_o);

--    --Conexiones de UART
--    --UART:
--    UART_i.rxPin <= <signal>;-- pin de rx en placa
--    UART_i.readStrobe <= <signal>;-- leer el siguiente dato de UART 
--    UART_i.TxData <= <signal>;-- Dato a escribir en UART  
--    UART_i.TxStrobe <= <signal>;-- Strobe para escribir en UART
--    <signal> <= UART_o.TxPin; --pin de tx en placa
--    <signal> <= UART_o.RxData;--dato de llegada
--    <signal> <= UART_o.RxEmpty; 
--    <signal> <= UART_o.TxFull;
---------------------------------------------------------------
-- Version		Authors				Date				Changes
-- 1.0                                                  Initial
-- 2.0 			CCFF                7/8/2020            generico, cualquier Baudrate
------------------------------------------------

entity UART is
    generic(
        BAUDRATE : natural :=19200;--baudrate real en bps
        CLK_FREQ : natural :=100000000;--reloj de sistema en MHz
        TICKS_P_BIT : natural :=16;
        SB_TICK : natural :=16; -- normalmente igual a TICKS_P_BIT, en altas velocidades TICKS_P_BIT*2
        N : natural := 9 -- Round this parameter from excel sheet  (CLK_frec/(TICKS_P_BIT * BAUDRATE))
    );
    Port ( clk : in STD_LOGIC;
           reset : in std_logic;
           UART_i : in UART_iType;
           UART_o : out UART_oType
    );
end UART;

architecture Behavioral of UART is
	
    --baudrat grator se�al
    signal br_tick : std_logic;

    -- UART Rx
    signal RxUart_i :  RxUart_itype;
    signal RxUart_o :  RxUart_otype ; 
    
    -- UART Tx
    signal TxUart_i :  TxUart_itype;
    signal TxUart_o :  TxUart_otype ; 
    
    -- FIFO Tx
    signal TxFIFO_empty,TxFIFO_rd,TxFIFO_WR_EN,TxFIFO_FULL : std_logic;
    signal TxFIFO_DIN,TxFIFO_dout : std_logic_vector(7 downto 0);
    
    -- FIFO Rx
    signal RxFIFO_empty,RxFIFO_rd,RxFIFO_WR_EN,RxFIFO_FULL : std_logic;
    signal RxFIFO_DIN,RxFIFO_dout : std_logic_vector(7 downto 0);
    
begin

    --Iniciacion de Baudrate Genrator   
    -- N : Baudrate gen constante 
    br_gen_inst : entity work.BR_gen 
    generic map(
        N=>N
    ) 
    port map (
         CLK   => clk,
         br_tick  => br_tick 
    );
    
    --Iniciacion de UART Rx
    Rx_UART : entity work.uart_rx     
    generic map(
        DBIT=>8,     -- # data bits, por defecto 8 bits de transmision
	    TICKS_P_BIT=>TICKS_P_BIT,     -- # ticks por bit
        SB_TICK=>SB_TICK -- # ticks for stop bits, normalmente deberia ser TICKS_P_BIT, pero en alta velocidad, el tiempo de stop es mayor
    )
    port map (
        clk,reset,RxUart_i,RxUart_o      
    );
	 
      
    --Iniciacion de FIFO UART Rx
    Rx_FIFO : entity work.FIFO16x8_bb 	 
    port map (
        CLK   => clk,
        reset  => reset,
        datain   => RxFIFO_DIN,
        enr => RxFIFO_RD,
        enw => RxFIFO_wr_en,
        dataout  => RxFIFO_dout,
        empty => RxFIFO_empty,
        full  => open		  
    );    
	 
	 
    --Iniciacion de txUart
    Tx_UART : entity work.uart_tx  
    generic map(
        DBIT=>8,     -- # data bits, por defecto 8 bits de transmision
	    TICKS_P_BIT=>TICKS_P_BIT,     -- # ticks por bit
        SB_TICK=>SB_TICK -- # ticks for stop bits, normalmente deberia ser TICKS_P_BIT, pero en alta velocidad, el tiempo de stop es mayor
    )
    port map(
        clk,--reloj de sistema
        reset,--reset de sistema
        txUART_i,
        txUART_o      
    ); 
    --Iniciacion de FIFO UART Tx
    Tx_FIFO : entity work.FIFO16x8_bb	
    port map (
     CLK   => clk,
     reset  => reset,
     datain   => TxFIFO_din,
     enr => TxFIFO_rd,
     enw => TxFIFO_wr_en,
     dataout  => TxFIFO_dout,  
     empty => TxFIFO_empty,
     full  => TxFIFO_full
    );
    -----------------------------------------------------
    --Conexiones
    -----------------------------------------------------
    --RxUART:
    RxUart_i.rx<=UART_i.rxPin;
    RxUart_i.s_tick<=br_tick;--baudrate
    --fifo rx:   
    RxFIFO_din<=RxUart_o.dout;--datos de salida de UART entran a RxFIFO
    RxFIFO_wr_en<=RxUart_o.rx_done_tick;--Cuando termine de recibir los guarda    
    RxFIFO_rd <= UART_i.readStrobe;--strobe para leer el siguiente dato       
    --TxUART   
    txUART_i.s_tick<=br_tick;--del generador de baudrate
    txUART_i.tx_start_tick<=not TxFIFO_empty;--inicia nueva transmision cuando no este vacia la FIFO de Tx
    txUART_i.din<=TxFIFO_dout;--el dato que envia es el que este guardado en FIFO    
    --fifo Tx 
    TxFIFO_rd <= txUART_o.tx_done_tick;--Despues de transmitir lee si hay un dato pendiente por enviar
    TxFIFO_din<=UART_i.TxData;--El dato a transmitir se guarda en TxFIFO     
    TxFIFO_WR_EN <= UART_i.TxStrobe;    
    --Salidas 
    UART_o.RxData <= RxFIFO_DOUT;   --dato leido
    UART_o.RxEmpty <= RxFIFO_EMPTY;--no hay datos
    UART_o.txPin <= txUART_o.tx;--Pin de Tx Salida UART 
    UART_o.TxFull <= TxFIFO_FULL;
end Behavioral;
