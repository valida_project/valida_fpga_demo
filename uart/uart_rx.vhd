----------------------------------------------------------------------------------
-- Fecha : 13/03/2016
-- Datos personales : Christiam Franco, Y1475493B
-- Codigo de test para MSEEI 2016, FPGA
-- UART Rx
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.Rx_UART_pack.all; 
--------------------------------------------------------
---- Template de Iniciacion
----Iniciacion de UART Rx
--    Rx_UART : entity work.uart_rx     
--    port map (
--      clk=>clk,--reloj
--      reset=>reset,--reset
--      rx=>rx,--Entrada de linea de UART
--      s_tick=>br_tick,--Entrada de generador de baudrate
--      rx_done_tick=>rx_done_tick,--Strobe para avisar que ha llegado un dato
--      dout=> uartRx_data--Dato que ha llegado
--    );
-----------------------------------------------------------
entity uart_rx is
   generic(
      DBIT: integer:=8;     -- # data bits
		TICKS_P_BIT : integer:=6;     -- # ticks por bit
		SB_TICK: integer:=16  -- # ticks for stop bits
   );
   port(
      clk, reset: in std_logic;
      RxUart_i : in RxUart_iType;
      RxUart_o : out  RxUart_oType     
   );
end uart_rx ;


architecture arch of uart_rx is
    type state_type is (idle, start, data, stop);
    type reg_type is record
        state : state_type;
        s : unsigned(3 downto 0);
        n : unsigned(2 downto 0);  
        b : std_logic_vector(7 downto 0);   
    end record;
    signal r,r_next : reg_type; 
    signal uart_Rxd_q,uart_Rxd_q1 : std_LOGIC;
begin

	-- Para evitar metastibilidad en entradas asincronas
	-- Puede causar problemas 
	process(clk)
	begin
		if rising_edge(clk) then
			uart_Rxd_q <= RxUart_i.rx;
			uart_Rxd_q1 <= uart_Rxd_q;
		end if;
	end process;


   -- FSMD state & data registers
   process(clk)
   begin
      if (clk'event and clk='1') then
         r<=r_next;        
      end if;
   end process;
   -- next-state logic & data path functional units/routing
   process(r,RxUart_i.s_tick,uart_Rxd_q1)
   begin
      r_next<=r;      
      RxUart_o.rx_done_tick <='0';
      case r.state is
         when idle =>

            if uart_Rxd_q1='0' then
               r_next.state <= start;
               r_next.s <= (others=>'0');
            end if;
         when start =>
            if (RxUart_i.s_tick = '1') then
               if r.s=(TICKS_P_BIT/2-1) then
                  r_next.state <= data;
                  r_next.s <= (others=>'0');
                  r_next.n <= (others=>'0');
               else
                  r_next.s <= r.s + 1;
               end if;
            end if;
         when data =>
            if (RxUart_i.s_tick = '1') then
               if r.s=TICKS_P_BIT-1 then
                  r_next.s <= (others=>'0');
                  r_next.b <= uart_Rxd_q1 & r.b(7 downto 1) ;
                  if r.n=(DBIT-1) then
                     r_next.state <= stop ;
                  else
                     r_next.n <= r.n + 1;
                  end if;
               else
                  r_next.s <= r.s + 1;
               end if;
            end if;
         when stop =>
            if (RxUart_i.s_tick = '1') then               
               if r.s=(SB_TICK-1) then
                  r_next.state <= idle;
                  RxUart_o.rx_done_tick <='1';
               else
                  r_next.s <= r.s + 1;
               end if;
            end if;
      end case;
   end process;
	
   RxUart_o.dout <= r.b;
end arch;




