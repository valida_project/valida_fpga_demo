----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 08.04.2016 23:30:32
-- Design Name: 
-- Module Name: Tx_UART_pack - 
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

package Tx_UART_pack is
    type TxUART_itype is record
        s_tick: std_logic;
        tx_start_tick: std_logic;
        din: std_logic_vector(7 downto 0);
    end record;
    type TxUART_otype is record
        tx: std_logic;
        tx_done_tick : std_logic;
    end record;
end Tx_UART_pack;


