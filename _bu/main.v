// --------------------------------------------------------------------------
// -- @Info:
// -- --------------------
// -- 
// --------------------------------------------------------------------------
// -- @History:
// -- Fecha		VERSION		Comments
// -- --------------------------------------------------

// -----------------------------------------------------
module main (
   // Flash
   input clk,
   input rst,

   output o_i2s_clk,
   input  i_i2s_din,
   output o_i2s_ws,

   input  i_uart_rx,
   output o_uart_tx,
   output [7:0] o_led
);

    wire [2:0] fsm_led;
    //-------------------------
    // AudioRAM signals
    //-------------------------
    localparam AUDIORAM_ADDR_WIDTH = 12;
    localparam AUDIORAM_DATA_WIDTH = 8;
    wire audioRAM_wr_en_i;	
    wire [AUDIORAM_ADDR_WIDTH-1:0] audioRAM_wr_addr_i;
    wire [AUDIORAM_DATA_WIDTH-1:0] audioRAM_wr_data_i;
    wire audioRAM_rd_en_i;	
    wire [AUDIORAM_ADDR_WIDTH-1:0] audioRAM_rd_addr_i;
    wire [AUDIORAM_DATA_WIDTH-1:0] audioRAM_rd_data_o;    

    //-------------------------
    // AudioRAM Instantiation
    //-------------------------
    single_port_ram 
    #(  .ADDR_WIDTH(AUDIORAM_ADDR_WIDTH), 
        .DATA_WIDTH(AUDIORAM_DATA_WIDTH)
    )
    ram_audio_inst (
        .clk           (clk), 
        .wr_en_i       (audioRAM_wr_en_i), 
        .wr_addr_i     (audioRAM_wr_addr_i), 
        .wr_data_i     (audioRAM_wr_data_i), 
        .rd_en_i       (audioRAM_rd_en_i),        
        .rd_addr_i     (audioRAM_rd_addr_i), 		
        .rd_data_o     (audioRAM_rd_data_o)     
    ) ;   
    
    //---------------------------------
    // FSM communication UART Layer
    //---------------------------------
    CommLayer 
    #(  .RAM_EXT_ADD_WIDTH  (AUDIORAM_ADDR_WIDTH), 
        .RAM_EXT_DATA_WIDTH (AUDIORAM_DATA_WIDTH)
    )
    interface_inst (
        .clk(clk),
        .rst(rst),

        .led (fsm_led),

        .i_uart_rx (i_uart_rx),
        .o_uart_tx (o_uart_tx),

        .audioRAM_wr_addr_i  (audioRAM_wr_addr_i), 
        .audioRAM_wr_data_i  (audioRAM_wr_data_i),        
        .audioRAM_wr_en_i    (audioRAM_wr_en_i), 	
        .audioRAM_rd_addr_i  (audioRAM_rd_addr_i), 	
        .audioRAM_rd_data_o  (audioRAM_rd_data_o), 	
        .audioRAM_rd_en_i    (audioRAM_rd_en_i),

        .audio_i_smp_we (audio_o_smp_we),
        .audio_i_ld (audio_o_ld)
    );

    // for Audio module
    wire [15:0] audio_o_ld;
    wire audio_o_smp_we;
    wire audio_o_active;
    //---------------------------------
    // MIC driver I2S
    //---------------------------------
    i2sm_in #(.GAIN(4), .DIV(98))
    u_i2sm_in ( 
        .clk(clk),
        .resetn(rst_n),
        .o_ld (),
        .o_rd (),
        .o_smp_we (audio_o_smp_we),
        .i_dc_value ('hE42E), // -7122
        .i_level_th (100),        
        .o_active (audio_o_active), 	
        .o_ld_with_dc (audio_o_ld),
        .o_bclk (o_i2s_clk), 	
        .i_sd (i_i2s_din),	
        .o_ws (o_i2s_ws)
    );

    //---------------------------------
    // AFE
    //---------------------------------
    wire afe_i_enable;
    wire [RC_PRECISION - 1:0] afe_i_senyal_u;
    wire afe_o_out_enable;
    wire afe_o_weights;

    afe inst_afe ( 
        .clk            (clk),
        .i_nreset       (rst_n),
        .i_enable       (afe_i_enable),
        .i_senyal_u     (afe_i_senyal_u),
        .o_out_enable   (afe_o_out_enable),
        .o_weights      (afe_o_weights)        
    );
   

    assign rst_n = ~rst;
    assign o_led = {4'b1010, fsm_led, audio_o_active}; 

endmodule