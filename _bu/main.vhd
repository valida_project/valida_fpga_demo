library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

--------------------------------------------------------------------------
-- @Info:
-- --------------------
-- 
--------------------------------------------------------------------------
-- @History:
-- Fecha		VERSION		Comments
-- --------------------------------------------------

-----------------------------------------------------
entity main is 
    
    Port ( 	clk             : in std_logic;		
            rst             : in std_logic;
            -- Pines i2s
            o_i2s_clk       : out std_logic;
            i_i2s_din       : in  std_logic;
            o_i2s_ws        : out std_logic;
            --Pines de uart
            i_uart_rx       : in  std_logic;
            o_uart_tx       : out std_logic;
            -- led
            o_led           : out std_logic_vector(7 downto 0)			
    );
end main;

architecture rtl of main is
    constant RAM_EXT_ADD_WIDTH      : integer := 12;
    constant RAM_EXT_DATA_WIDTH     : integer := 8;
    
    signal fsm_led                  :  std_logic_vector(2 downto 0);
    signal fsm_audioRAM_wr_addr_i 	:  std_logic_vector(RAM_EXT_ADD_WIDTH-1 downto 0);
    signal fsm_audioRAM_wr_data_i 	:  std_logic_vector(RAM_EXT_DATA_WIDTH-1 downto 0);  
    signal fsm_audioRAM_wr_en_i 	:  std_logic;
    signal fsm_audioRAM_rd_addr_i 	:  std_logic_vector(RAM_EXT_ADD_WIDTH-1 downto 0);
    signal fsm_audioRAM_rd_data_o 	:  std_logic_vector(RAM_EXT_DATA_WIDTH-1 downto 0);  
    signal fsm_audioRAM_rd_en_i 	:  std_logic;
    signal fsm_audio_i_smp_we		:  std_logic;
    signal fsm_audio_i_ld			:  std_logic_vector(15 downto 0);

    -- audio i2s
    signal audio_o_active : std_logic;

begin

    spRam_inst : entity work.single_port_ram
    generic map(RAM_EXT_ADD_WIDTH, RAM_EXT_DATA_WIDTH)
    port map (
        clk, 
        fsm_audioRAM_wr_en_i,
        fsm_audioRAM_wr_addr_i,
        fsm_audioRAM_wr_data_i,
        fsm_audioRAM_rd_en_i,
        fsm_audioRAM_rd_addr_i,
        fsm_audioRAM_rd_data_o
    );

    -----------------------------------
	-- FSM communication UART Layer
	-----------------------------------
    CommLayer_inst : entity work.CommLayer 
    generic map(RAM_EXT_ADD_WIDTH   => RAM_EXT_ADD_WIDTH, 
                RAM_EXT_DATA_WIDTH  => RAM_EXT_DATA_WIDTH)   
    port map (  
        clk, 
        rst, 
        fsm_led, 
        i_uart_rx, 
        o_uart_tx,
        fsm_audioRAM_wr_addr_i, fsm_audioRAM_wr_data_i, fsm_audioRAM_wr_en_i,
        fsm_audioRAM_rd_addr_i, fsm_audioRAM_rd_data_o, fsm_audioRAM_rd_en_i,
        fsm_audio_i_smp_we, fsm_audio_i_ld
    );
            
    -----------------------------------
	-- MIC driver I2S
	-----------------------------------
    I2S_mic_inst : entity work.i2sm_in
    --generic map(GAIN=>4, DIV=>98)
	port map (	
        clk,
        rstn,
        open,
        open,
        fsm_audio_i_smp_we,
        X"E42E", -- i_dc_value = -7122
        X"0064", -- i_level_th = 100   
        audio_o_active, 	
        fsm_audio_i_ld,
        o_i2s_clk, 	
        i_i2s_din,	
        o_i2s_ws	
	);

	
    rstn <= not rst;
    -- output connection
    o_led <= "1010" & fsm_led & audio_o_active;

end rtl;

