
# Entity: main 
- **File**: main.vhd

## Diagram
![Diagram](main.svg "Diagram")
## Generics

| Generic name           | Type    | Value | Description |
| ---------------------- | ------- | ----- | ----------- |
| AUDIO_GAIN             | integer | 4     |             |
| AUDIO_OFFSET           | integer | -7117 |             |
| AUDIO_THRESH           | integer | 150   |             |
| AUDIO_RESOLUTION_I     | integer | 6     |             |
| VALIDA_EXP_DELTA       | integer | 1     |             |
| VALIDA_MAC_THRESH_HIGH | integer | 4000  |             |
| VALIDA_MAC_THRESH_LOW  | integer | 20000 |             |

## Ports

| Port name | Direction | Type                         | Description |
| --------- | --------- | ---------------------------- | ----------- |
| clk       | in        | std_logic                    |             |
| rst       | in        | std_logic                    |             |
| o_i2s_clk | out       | std_logic                    |             |
| i_i2s_din | in        | std_logic                    |             |
| o_i2s_ws  | out       | std_logic                    |             |
| i_uart_rx | in        | std_logic                    |             |
| o_uart_tx | out       | std_logic                    |             |
| i_train   | in        | std_logic                    |             |
| o_led     | out       | std_logic_vector(7 downto 0) |             |

## Signals

| Name                     | Type                                            | Description |
| ------------------------ | ----------------------------------------------- | ----------- |
| rstn                     | std_logic                                       |             |
| fsm_led                  | std_logic_vector(2 downto 0)                    |             |
| fsm_audioRAM_wr_addr_i   | std_logic_vector(RAM_EXT_ADD_WIDTH-1 downto 0)  |             |
| fsm_audioRAM_wr_data_i   | std_logic_vector(RAM_EXT_DATA_WIDTH-1 downto 0) |             |
| fsm_audioRAM_wr_en_i     | std_logic                                       |             |
| fsm_audioRAM_rd_addr_i   | std_logic_vector(RAM_EXT_ADD_WIDTH-1 downto 0)  |             |
| fsm_audioRAM_rd_data_o   | std_logic_vector(RAM_EXT_DATA_WIDTH-1 downto 0) |             |
| fsm_audioRAM_rd_en_i     | std_logic                                       |             |
| fsm_afe_input_control    | std_logic                                       |             |
| fsm_afe_i_enable         | std_logic                                       |             |
| fsm_afe_i_senyal_u       | signed ((rc_precision -1) downto 0)             |             |
| audio_i_gain             | std_logic_vector(4 downto 0)                    |             |
| audio_i_offset           | std_logic_vector(15 downto 0)                   |             |
| audio_i_level_th         | std_logic_vector(15 downto 0)                   |             |
| audio_o_active           | std_logic                                       |             |
| audio_o_en               | std_logic                                       |             |
| audio_o_data             | std_logic_vector(15 downto 0)                   |             |
| audio_resolution         | std_logic_vector(3 downto 0)                    |             |
| audio_resolution_int     | integer range 0 to 15                           |             |
| audio_data_cropped       | signed(7 downto 0)                              |             |
| valida_i_nreset          | std_logic                                       |             |
| valida_i_enable          | std_logic                                       |             |
| valida_i_senyal_u        | signed ((rc_precision -1) downto 0)             |             |
| afe_o_out_enable         | std_logic                                       |             |
| afe_o_weights            | unarray_plus ((n+1) downto 1)                   |             |
| valida_o_y_out           | std_logic                                       |             |
| valida_o_detect          | std_logic                                       |             |
| valida_o_state_bit       | std_logic                                       |             |
| valida_o_reservoir       | signed(2*BIT_PRECISION - 1 DOWNTO 0)            |             |
| valida_o_internal_enable | std_logic                                       |             |
| valida_i_exp_delta       | INTEGER RANGE 0 TO 3                            |             |
| valida_i_mac_thresh_high | INTEGER RANGE 0 TO 262143                       |             |
| valida_i_mac_thresh_low  | INTEGER RANGE 0 TO 262143                       |             |

## Constants

| Name                    | Type      | Value                                                                              | Description |
| ----------------------- | --------- | ---------------------------------------------------------------------------------- | ----------- |
| RAM_EXT_ADD_WIDTH       | integer   | 12                                                                                 |             |
| RAM_EXT_DATA_WIDTH      | integer   | 8                                                                                  |             |
| AFE_INPUT_MIN_VALUE     | signed    | to_signed(-2**(RC_PRECISION-1),<br><span style="padding-left:20px"> RC_PRECISION)  |             |
| AFE_INPUT_MAX_VALUE     | signed    | to_signed(2**(RC_PRECISION-1)-1,<br><span style="padding-left:20px"> RC_PRECISION) |             |
| AFE_INPUT_CONTROL_MICRO | std_logic | '0'                                                                                |             |
| AFE_INPUT_CONTROL_FSM   | std_logic | '1'                                                                                |             |

## Processes
- unnamed: ( clk, audio_resolution_int, audio_o_data )

## Instantiations

- I2S_mic_inst: i2sm_in
- CommLayer_inst: work.CommLayer
- spRam_inst: work.single_port_ram
- inst_afe: work.valida
