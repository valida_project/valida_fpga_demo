library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
--USE IEEE.STD_LOGIC_ARITH.ALL;
--USE IEEE.STD_LOGIC_UNSIGNED.ALL;
--use work.mytypes.all;
use work.UART_pack.all;
USE WORK.mytypes.ALL;
--------------------------------------------------------------------------
-- @Info:
-- --------------------
-- Este fichero tiene una maquina de estados que sirve para testear
-- la uart y tambien para poder empezar a realizar una APPI en la FPGA.

-- @funcionamiento:
-- --------------------
-- Cuando el sistema inicia imprime por UART "CONFIG_BUFFER_LEN" veces el valor
-- 105 para identificarse. 
-- Despues se queda esperando a que le llegue algun dato por la uart.
-- Si el dato tiene el valor de algunos de los comandos validados, entonces
-- ejecuta el comando. En caso contrario, realiza un ECO del valor.
--
-- Buffer data: 
-------------------
-- El sistema tiene 1 buffer de CONFIG_BUFFER_LEN size. En este buffer se pueden
-- guardar datos de configuración o de utilidad para la herramienta.
-- Es importante senalar la diferencia entre el Config_buffer y la RAM externa
-- utilizada para almacenar datos y leerlos. 
--
-- Importante: Este modulo se debe configurar con el excel sheet. 
--------------------------------------------------------------------------
-- @History:
-- Fecha		VERSION		Comments
-- 02/06/2021	1			testeado y funciona con una FPGA lattice.
--							Lo he dejado listo para funcionar.
-- 28/07/2021	2			Baudrate 230400
-- 
-----------------------------------------------------
entity CommLayer is 
    generic(
        VERSION : natural :=3;                      -- Version enviada al preguntar por UART
        
        BAUDRATE        : natural := 230400;	    -- baudrate real en bps
        CLK_FREQ        : natural := 50000000;	    -- reloj de sistema en Hz
        TICKS_P_BIT     : natural := 7;
        SB_TICK         : natural := 7;				-- equal to TICKS_P_BIT, for high speed = TICKS_P_BIT*2
        UART_N          : natural := 31;			-- Round this parameter from excel sheet  (CLK_frec/(TICKS_P_BIT * BAUDRATE))
    
        CONFIG_BUFFER_LEN : integer := 64;          -- tamaño de Config buffer

        RAM_EXT_ADD_WIDTH : integer := 12;
        RAM_EXT_DATA_WIDTH : integer := 8
    );
    Port ( 	clk : in STD_LOGIC;		
            rst : in std_logic;
            
            led : out std_logic_vector(2 downto 0);
            --Pines de uart
            i_uart_rx : in std_LOGIC;
            o_uart_tx : out std_LOGIC;
            -- Pines de RAM audio
            audioRAM_wr_addr_i 	: out std_logic_vector(RAM_EXT_ADD_WIDTH-1 downto 0);
            audioRAM_wr_data_i 	: out std_logic_vector(RAM_EXT_DATA_WIDTH-1 downto 0);  
            audioRAM_wr_en_i 	: out std_logic;
            audioRAM_rd_addr_i 	: out std_logic_vector(RAM_EXT_ADD_WIDTH-1 downto 0);
            audioRAM_rd_data_o 	: in std_logic_vector(RAM_EXT_DATA_WIDTH-1 downto 0);  
            audioRAM_rd_en_i 	: out std_logic;
            -- Pines de entrada audio
            audio_o_en		    : in  std_logic;
            audio_o_data	    : in  std_logic_vector(15 downto 0);
            audio_i_gain        : out std_logic_vector(4  downto 0);
            audio_i_offset      : out std_logic_vector(15 downto 0);
            audio_i_level_th    : out std_logic_vector(15 downto 0);
            audio_resolution    : out std_logic_vector(3 downto 0);
            -- cropped audio
            audio_data_cropped  : in  signed ((rc_precision -1) downto 0);
            -- Puertos de afe
            afe_input_control   : out std_logic;
            afe_i_nreset        : out std_logic;
            afe_i_enable        : out std_logic;
            afe_i_senyal_u      : out signed ((rc_precision -1) downto 0);
            afe_o_out_enable    : in  std_logic;
            afe_o_weights       : in  unarray_plus ((n+1) downto 1);
            valida_i_exp_delta       : out INTEGER RANGE 0 TO 3;
            valida_i_mac_thresh_high : out INTEGER RANGE 0 TO 32767;
            valida_i_mac_thresh_low  : out INTEGER RANGE 0 TO 32767;
            -- valida
            valida_o_detect     : in std_logic;
            valida_o_state_bit  : in std_logic;
            valida_o_y_out      : in std_logic;
            valida_o_reservoir  : in signed(2*BIT_PRECISION - 1 DOWNTO 0);
            valida_o_internal_enable : in std_logic

    );
end CommLayer;

architecture rtl of CommLayer is

    -- Numero de ciclos de retardo que equivalen a 1 byte en UART
    constant CYCLES_1_BYTE_UART : integer := (11*CLK_FREQ/BAUDRATE -1);
    constant VALIDA_DATA_BUFFER_LEN : integer := 10;

    signal UART_i : UART_iType;
    signal UART_o : UART_oType;	

    -- Estados de FSM
    type state_t is ( 	ST_BOOT, ST_IDDLE,						
                        ST_VALID_CONFIG_HEADER, ST_READ_N_PACKETS, ST_READ_ALL_DATA, 
                        ST_SEND_DATA_BUFFER,
                        ST_RAM_WR, ST_RAM_WR_1,  ST_RAM_WR_2,
                        ST_RAM_RD, ST_RAM_RD_1, ST_RAM_RD_2,
                        -- Audio sample states
                        ST_AUDIO_READ_N_SAMPLES_MSB, ST_AUDIO_READ_N_SAMPLES_LSB,
                        ST_AUDIO_SAMPLE_NOW, ST_AUDIO_SEND_SAMPLE_MSB, ST_AUDIO_SEND_SAMPLE_LSB,
                        -- AFE sample states
                        ST_AFE_READ_N_SAMPLES_MSB, ST_AFE_READ_N_SAMPLES_LSB, ST_AFE_SAMPLE_NOW,
                        ST_AFE_SEND_N_SAMPLES,
                        -- AfE Test
                        ST_AFE_TEST_N_SAMPLES_MSB, ST_AFE_TEST_N_SAMPLES_LSB,
                        ST_AFE_TEST_READ_CSV, ST_AFE_TEST_SEND_N_SAMPLES,
                        ST_AFE_TEST_SEND_CSV,
                        -- Send Valida out states
                        ST_VALIDA_WAIT_STROBE, ST_VALIDA_SEND_DATA,
                        -- Varios
                        GET_UART, SEND_UART,ST_SEND_ECO, WAIT_CYCLES);

    -- declaramos RxBuffer e indices
    type data_buffer_t is ARRAY(0 to CONFIG_BUFFER_LEN-1) OF std_logic_vector(7 downto 0);
    CONSTANT CONFIG_BUFFER_IDX_AUDIO_GAIN_UINT8         : integer := 0;
    CONSTANT CONFIG_BUFFER_IDX_AUDIO_OFFSET_UINT16      : integer := 1;
    CONSTANT CONFIG_BUFFER_IDX_AUDIO_THRESH_UINT16      : integer := 3;
    CONSTANT CONFIG_BUFFER_IDX_AUDIO_RES_UINT8          : integer := 5;
    CONSTANT CONFIG_BUFFER_IDX_VALIDA_EXP_DELTA_UINT8   : integer := 6;
    CONSTANT CONFIG_BUFFER_IDX_VALIDA_MAC_THRESH_HIGH_UINT16    : integer := 7;
    CONSTANT CONFIG_BUFFER_IDX_VALIDA_MAC_THRESH_LOW_UINT16     : integer := 9;

    -- Signals to control AFE input
    CONSTANT AFE_INPUT_CONTROL_MICRO : std_logic := '0';
    CONSTANT AFE_INPUT_CONTROL_FSM   : std_logic := '1';
    --señales FSM
    type fsm_type is record	
        state : state_t;
        --Estado de retorno 
        stateRet : state_t;
        stateRet_WC : state_t;
        --Variable para Estado WAIT_CYCLES
        --Su valor depende del maximo retardo que realizara
        --Para este caso esperara hasta enviar un byte de UART
        --por esto depende del baudrate.
        --Maximo retardo 11 bits por seguridad,8bits + 1stop + 1Start + 1seguridad
        numCyclesWait : integer range 0 to 11*CLK_FREQ/BAUDRATE;
        
        -- global
        cont : integer range 0 to CONFIG_BUFFER_LEN-1;	
        n_packets :	integer range 0 to 2**RAM_EXT_ADD_WIDTH-1;
        led : std_logic_vector(2 downto 0);
        send_echo : std_logic;
        -- UART
        readStrobe : std_logic;
        RxDataUART : std_logic_vector(7 downto 0);
        TxStrobe : std_logic;
        TxDataUART : std_logic_vector(7 downto 0);			
        configBuffer : data_buffer_t;
        -- External RAM
        audioRAM_wr_addr_i 	:  integer range 0 to 2**RAM_EXT_ADD_WIDTH-1;
        audioRAM_wr_data_i 	:  std_logic_vector(audioRAM_wr_data_i'left downto 0); 
        audioRAM_wr_en_i 	:  std_logic;
        audioRAM_rd_addr_i 	:  integer range 0 to 2**RAM_EXT_ADD_WIDTH-1;
        audioRAM_rd_en_i 	:  std_logic;

        -- Audio
        audio_n_samples         : unsigned(15 downto 0);
        audio_cont_samples      : unsigned(15 downto 0);
        audio_data_in           : std_logic_vector(15 downto 0);
        -- Audio cropped
        audio_cropped_flag      : std_logic;
        -- AFE
        afe_i_nreset : std_logic;
        afe_cont_n : integer range 1 to N+1;
        afe_i_enable : std_LOGIC;
        afe_input_control : std_logic;
        
    end record;

    signal r, rn : fsm_type;

    signal valida_i_mac_thresh_high_slv : std_logic_vector(15 downto 0);
    signal valida_i_mac_thresh_low_slv  : std_logic_vector(15 downto 0);

    type valida_data_buffer_t is ARRAY(0 to VALIDA_DATA_BUFFER_LEN-1) OF std_logic_vector(7 downto 0);
    signal valida_data_buffer : valida_data_buffer_t;

begin

    ---------------------------------------------
    --Conexiones UART
    ---------------------------------------------
    --DEclaracion para modulo UART
    uart_inst : entity work.UART
    generic map(BAUDRATE , CLK_FREQ, TICKS_P_BIT, SB_TICK, UART_N)
    port map(clk, rst, UART_i, UART_o);
    
    --UART Rx:
    UART_i.rxPin 		<= i_uart_rx;-- pin de rx en placa
    UART_i.readStrobe 	<= r.readStrobe;

    --UART Tx:
    o_uart_tx <= UART_o.TxPin; --pin de tx en placa
    UART_i.TxData <= r.TxDataUART;-- Dato a escribir en UART  por FSM	 
    UART_i.TxStrobe <= r.TxStrobe;	  

    -- RAM
    audioRAM_wr_addr_i 	<= std_logic_vector(to_unsigned(r.audioRAM_wr_addr_i, audioRAM_wr_addr_i'length)); 
    audioRAM_wr_data_i 	<= r.audioRAM_wr_data_i;  
    audioRAM_wr_en_i 	<= r.audioRAM_wr_en_i;
    audioRAM_rd_addr_i 	<= std_logic_vector(to_unsigned(r.audioRAM_rd_addr_i, audioRAM_rd_addr_i'length)); 
    audioRAM_rd_en_i 	<= r.audioRAM_rd_en_i;
    
    -- AFE
    afe_i_nreset <= r.afe_i_nreset;
    afe_i_enable <= r.afe_i_enable;
    afe_input_control <= r.afe_input_control;
    -- i2s module
    audio_i_gain                    <= r.configBuffer(CONFIG_BUFFER_IDX_AUDIO_GAIN_UINT8)(audio_i_gain'range);
    audio_i_offset(15 downto 8)     <= r.configBuffer(CONFIG_BUFFER_IDX_AUDIO_OFFSET_UINT16);
    audio_i_offset(7  downto 0)     <= r.configBuffer(CONFIG_BUFFER_IDX_AUDIO_OFFSET_UINT16 + 1);
    audio_i_level_th(15 downto 8)   <= r.configBuffer(CONFIG_BUFFER_IDX_AUDIO_THRESH_UINT16);
    audio_i_level_th(7  downto 0)   <= r.configBuffer(CONFIG_BUFFER_IDX_AUDIO_THRESH_UINT16 + 1);
    audio_resolution                <= r.configBuffer(CONFIG_BUFFER_IDX_AUDIO_RES_UINT8)(audio_resolution'range);     
    valida_i_exp_delta              <= to_integer(unsigned(r.configBuffer(CONFIG_BUFFER_IDX_VALIDA_EXP_DELTA_UINT8)));     
    valida_i_mac_thresh_high_slv(15 downto 8) <= r.configBuffer(CONFIG_BUFFER_IDX_VALIDA_MAC_THRESH_HIGH_UINT16);     
    valida_i_mac_thresh_high_slv(7  downto 0) <= r.configBuffer(CONFIG_BUFFER_IDX_VALIDA_MAC_THRESH_HIGH_UINT16 + 1);     
    valida_i_mac_thresh_low_slv(15 downto 8)  <= r.configBuffer(CONFIG_BUFFER_IDX_VALIDA_MAC_THRESH_LOW_UINT16);     
    valida_i_mac_thresh_low_slv(7  downto 0)  <= r.configBuffer(CONFIG_BUFFER_IDX_VALIDA_MAC_THRESH_LOW_UINT16 + 1);     

    valida_i_mac_thresh_high <= to_integer(unsigned(valida_i_mac_thresh_high_slv));
    valida_i_mac_thresh_low  <= to_integer(unsigned(valida_i_mac_thresh_low_slv));
    
    -- data to export from VALIDA module
    process(clk)
    begin
        if (rising_edge(clk)) then
            -- marca de agua inicio packet
            valida_data_buffer(0) <= std_logic_vector(to_unsigned(255, valida_data_buffer(0)'length));
            valida_data_buffer(1) <= std_logic_vector(to_unsigned(0, valida_data_buffer(0)'length));
            valida_data_buffer(2) <= std_logic_vector(to_unsigned(255, valida_data_buffer(0)'length));
            valida_data_buffer(3) <= std_logic_vector(to_unsigned(0, valida_data_buffer(0)'length));
            -- valida_o_reservoir, 32 bits signed
            valida_data_buffer(4) <= std_logic_vector(valida_o_reservoir(31 downto 24));
            valida_data_buffer(5) <= std_logic_vector(valida_o_reservoir(23 downto 16));
            valida_data_buffer(6) <= std_logic_vector(valida_o_reservoir(15 downto 8));
            valida_data_buffer(7) <= std_logic_vector(valida_o_reservoir(7  downto 0));
            -- valida state bits
            valida_data_buffer(8) <= "00000" & valida_o_detect & valida_o_state_bit & valida_o_y_out;
            -- dummy bytes for future use
            valida_data_buffer(9) <= "00000000";
        end if;
    end process;

    -- Other
    led <= r.led;
    ---------------------------
    -- FSM 
    ---------------------------
        process(clk, rst)
        begin 
            if rst = '1' then
                r.state <= ST_BOOT;
                r.configBuffer <= (others=>(others=>'0'));
            elsif rising_edge(clk) then
                r <= rn;
            end if;
        end process;

        process(r, UART_o, audioRAM_rd_data_o, audio_o_en, audio_o_data, 
                afe_o_out_enable, afe_o_weights, audio_data_cropped, valida_o_internal_enable)
        begin
            --Default
            rn <= r;
            rn.TxStrobe <= '0';
            rn.readStrobe <='0';	
            rn.audioRAM_wr_en_i <= '0';
            rn.audioRAM_rd_en_i <= '1';
            rn.afe_i_nreset <= '1';
            rn.afe_i_enable <= '0';
            

            case r.state is
                ---------------------------------------------
                -- boot:
                -- si queremos enviar algun comando de "hallo".
                -- aqui enviamos 64 veces 'i'
                ---------------------------------------------
                when ST_BOOT =>
                    rn.TxDataUART <= std_logic_vector(to_unsigned(105, 8)); 
                    rn.state <= SEND_UART;
                    rn.stateRet <= ST_BOOT;
                    rn.cont <= r.cont + 1;
                    if (r.cont = CONFIG_BUFFER_LEN-1) then
                        rn.cont <= 0;
                        rn.state <= ST_VALIDA_WAIT_STROBE;
                    end if;
                
                ---------------------------------------------
                -- Valida Send
                ---------------------------------------------
                when ST_VALIDA_WAIT_STROBE =>	
                    rn.led <= "100";
                    rn.afe_input_control <= AFE_INPUT_CONTROL_MICRO;
                    if (valida_o_internal_enable = '1') then
                        rn.state <= ST_VALIDA_SEND_DATA;
                        rn.cont <= 0;
                    end if;
                
                ---------------------------------------------
                -- STATE send data buffer VALIDA:
                ---------------------------------------------
                when ST_VALIDA_SEND_DATA =>
                    rn.cont <= r.cont + 1;
                    rn.stateRet <= ST_VALIDA_SEND_DATA;
                    rn.TxDataUART <= valida_data_buffer(r.cont);
                    rn.state <= SEND_UART;
                    if r.cont = (VALIDA_DATA_BUFFER_LEN-1) then
                        rn.stateRet <= ST_VALIDA_WAIT_STROBE;
                    end if;		

                ---------------------------------------------
                -- Estado iddle 
                ---------------------------------------------
                when ST_IDDLE =>					
                    rn.led <= "100";
                    rn.afe_input_control <= AFE_INPUT_CONTROL_MICRO;
                    -- si debe recibir datos de config
                    if (UART_o.RxEmpty ='0') then
                        rn.state <= GET_UART;
                        -- rn.state <= ST_BOOT;
                        rn.send_echo <= '1';
                        rn.stateRet <= ST_VALID_CONFIG_HEADER;	
                        rn.cont <= 0;					
                    end if;

                --------------------------------------------
                --
                --
                -- EVALUACION DE COMANDOS
                --
                --
                ---------------------------------------------
                ---------------------------------------------
                -- STATE validacion de cabecera
                -- si el comando tiene como cabecera hacer
                -- algo puntual, aqui es donde lo declaramos
                ---------------------------------------------
                when ST_VALID_CONFIG_HEADER =>
                    
                    --------------------------------------
                    -- "?" = x3F
                    -- comando are you there?
                    -- responde  = version
                    --------------------------------------
                    if (r.RxDataUART = X"3F") then 						
                        rn.TxDataUART <= std_logic_vector(to_unsigned(VERSION, 8));
                        rn.state <= SEND_UART;
                        rn.stateRet <= ST_IDDLE;
                        rn.led <= "011";	
                        rn.send_echo <= '0';


                    --------------------------------------
                    -- "C" = 0x43
                    -- Escribir buffer config
                    --------------------------------------
                    elsif (r.RxDataUART = X"43") then 						
                        rn.state <= GET_UART;
                        rn.stateRet <= ST_READ_N_PACKETS;
                        rn.led <= "001";
                        rn.send_echo <= '1';
                    --------------------------------------
                    -- "c" = x63
                    -- Leer buffer config
                    --------------------------------------
                    elsif (r.RxDataUART = X"63") then -- 						
                        rn.state <= ST_SEND_DATA_BUFFER;
                        rn.cont <= 0;
                        rn.led <= "010";
                        rn.send_echo <= '1';


                    --------------------------------------
                    -- "W" = 0x57
                    -- Escribir buffer Data
                    --------------------------------------
                    elsif (r.RxDataUART = X"57") then 						
                        rn.state <= ST_RAM_WR;
                        rn.led <= "011";
                        rn.send_echo <= '0';
                    --------------------------------------
                    -- "w" = x77
                    -- Leer buffer Data
                    --------------------------------------
                    elsif (r.RxDataUART = X"77") then -- 					
                        rn.state <= ST_RAM_RD;
                        rn.led <= "100";
                        rn.send_echo <= '1';

                    --------------------------------------
                    -- "S" = 0x53
                    -- Sample Audio Original
                    --------------------------------------
                    elsif (r.RxDataUART = X"53") then 						
                        rn.state <= GET_UART;
                        rn.stateRet <= ST_AUDIO_READ_N_SAMPLES_MSB;
                        rn.led <= "101";
                        rn.send_echo <= '1';
                        rn.audio_cont_samples <= (others=>'0');     
                        rn.audio_cropped_flag <= '0';               
                    --------------------------------------
                    -- "T" = 0x54
                    -- Sample AFE output
                    --------------------------------------
                    elsif (r.RxDataUART = X"54") then 						
                        rn.state <= GET_UART;
                        rn.stateRet <= ST_AFE_READ_N_SAMPLES_MSB;
                        rn.led <= "110";
                        rn.send_echo <= '1';
                        rn.audio_cont_samples <= (others=>'0');
                    --------------------------------------
                    -- "U" = 0x55
                    -- Test AFE 
                    --------------------------------------
                    elsif (r.RxDataUART = X"55") then 						
                        rn.state <= GET_UART;
                        rn.stateRet <= ST_AFE_TEST_N_SAMPLES_MSB;
                        rn.led <= "111";
                        rn.send_echo <= '0';
                        rn.afe_input_control <= AFE_INPUT_CONTROL_FSM;
                        rn.audio_cont_samples <= (others=>'0');
                    --------------------------------------
                    -- "V" = 0x56
                    -- Sample Audio Cropped
                    --------------------------------------
                    elsif (r.RxDataUART = X"56") then 						
                        rn.state <= GET_UART;
                        rn.stateRet <= ST_AUDIO_READ_N_SAMPLES_MSB;
                        rn.led <= "101";
                        rn.send_echo <= '1';
                        rn.audio_cont_samples <= (others=>'0');
                        rn.audio_cropped_flag <= '1';
                    else
                        rn.send_echo <= '1';
                        rn.state <= ST_IDDLE;
                    end if;
                    

                --------------------------------------------
                --
                --
                -- COMANDOS ACEPTADOS
                --
                --
                ---------------------------------------------
                -- STATE Write n packets in external RAM		
                -- Lee el # paquetes que viene en el valor despues
                -- de cabecera. 		
                ---------------------------------------------
                when ST_RAM_WR =>
                    
                    -- Siguiente estado
                    rn.state <= GET_UART;
                    rn.stateRet <= ST_RAM_WR_1;	
                    rn.audioRAM_wr_addr_i <= 2**RAM_EXT_ADD_WIDTH-1; -- maximo valor para que al sumar de 0
                    
                    when ST_RAM_WR_1 =>
                        -- 
                        rn.state <= ST_RAM_WR_2;
                        rn.audioRAM_wr_data_i <= r.RxDataUART;
                        rn.audioRAM_wr_addr_i <= r.audioRAM_wr_addr_i + 1;						
                    
                    when ST_RAM_WR_2 =>
                        -- 
                        rn.audioRAM_wr_en_i <= '1';						

                        -- Siguiente estado
                        rn.state <= GET_UART;
                        rn.stateRet <= ST_RAM_WR_1;	

                        if r.audioRAM_wr_addr_i = (2**RAM_EXT_ADD_WIDTH-1) then
                            rn.state <= ST_IDDLE;
                        end if;	

                        
                -----------------------------------------------
                -- STATE Read n packets in external RAM		
                --  		
                ---------------------------------------------
                when ST_RAM_RD =>					
                    rn.audioRAM_rd_addr_i <= 0; 
                    --Espera un tiempo para leer el dato
                    rn.numCyclesWait <= 2; 
                    rn.state <= WAIT_CYCLES;
                    rn.stateRet_WC <= ST_RAM_RD_1;

                    when ST_RAM_RD_1 =>
                        rn.TxDataUART <= audioRAM_rd_data_o;
                        rn.state <= SEND_UART;
                        rn.stateRet <= ST_RAM_RD_2;

                        if r.audioRAM_rd_addr_i = (2**RAM_EXT_ADD_WIDTH-1) then
                            rn.stateRet <= ST_IDDLE;
                        end if;

                    when ST_RAM_RD_2 =>
                        rn.audioRAM_rd_addr_i <= r.audioRAM_rd_addr_i + 1; 
                        --Espera un tiempo para leer el dato
                        rn.numCyclesWait <= 2;
                        rn.state <= WAIT_CYCLES;
                        rn.stateRet_WC <= ST_RAM_RD_1;

                                            
                    
                -----------------------------------------------
                -- STATE read n packets		
                -- Lee el # paquetes que viene en el valor despues
                -- de cabecera. 		
                ---------------------------------------------
                when ST_READ_N_PACKETS =>
                    rn.n_packets <= to_integer(unsigned(r.RxDataUART));
                    -- Siguiente estado
                    rn.state <= GET_UART;
                    rn.stateRet <= ST_READ_ALL_DATA;					
                    ---------------------------------------------
                    -- STATE read all data
                    ---------------------------------------------
                    when ST_READ_ALL_DATA =>
                        -- guarda en buffer de recepcion de UART
                        rn.configBuffer(r.cont) <= r.RxDataUART;
                        -- Siguiente estado
                        rn.state <= GET_UART;
                        rn.stateRet <= ST_READ_ALL_DATA;	
                        rn.cont <= r.cont + 1;
                        if r.cont = (r.n_packets-1) then
                            rn.state <= ST_IDDLE;
                        end if;	
                ---------------------------------------------
                -- STATE send data buffer:
                -- it sends the data read in the buffer
                ---------------------------------------------
                when ST_SEND_DATA_BUFFER =>
                    rn.cont <= r.cont + 1;
                    rn.stateRet <= ST_SEND_DATA_BUFFER;
                    rn.TxDataUART <= r.configBuffer(r.cont);
                    rn.state <= SEND_UART;
                    if r.cont = (CONFIG_BUFFER_LEN-1) then
                        --rn.state <= ST_IDDLE;
                        rn.stateRet <= ST_IDDLE;
                    end if;		
                ---------------------------------------------
                -- Estado envio de MEL
                ---------------------------------------------
                -- when ST_SEND_UK =>
                -- 	rn.TxDataUART <= std_logic_vector(i_uk(r.cont));
                -- 	rn.state <= SEND_UART;
                -- 	rn.stateRet <= ST_SEND_UK;
                -- 	rn.cont <= r.cont + 1;
                -- 	if (r.cont = 63) then
                -- 		rn.cont <= 0;
                -- 		rn.stateRet <= ST_IDDLE;
                -- 	end if;

                ---------------------------------------------
                -- STATE Samplea el audio
                --
                --
                ---------------------------------------------
                when ST_AUDIO_READ_N_SAMPLES_MSB =>
                    rn.audio_n_samples(15 downto 8) <= unsigned(r.RxDataUART);
                    -- Siguiente estado
                    rn.state <= GET_UART;
                    rn.stateRet <= ST_AUDIO_READ_N_SAMPLES_LSB;	
                when ST_AUDIO_READ_N_SAMPLES_LSB =>
                    rn.audio_n_samples(7 downto 0) <= unsigned(r.RxDataUART);
                    -- Siguiente estado
                    rn.state <= ST_AUDIO_SAMPLE_NOW;				
                when ST_AUDIO_SAMPLE_NOW =>
                    if (audio_o_en='1') then	
                        if (r.audio_cropped_flag = '0') then
                            rn.audio_data_in <= audio_o_data;				
                        else
                            rn.audio_data_in <= std_logic_vector(resize(audio_data_cropped, rn.audio_data_in'length));
                        end if;                        
                        rn.state <= ST_AUDIO_SEND_SAMPLE_MSB;
                    end if;
                    if (r.audio_cont_samples >= r.audio_n_samples) then
                        rn.state <= ST_IDDLE;
                    end if;
                when ST_AUDIO_SEND_SAMPLE_MSB =>					
                    rn.TxDataUART <= r.audio_data_in(15 downto 8); 
                    rn.state <= SEND_UART;
                    rn.stateRet <= ST_AUDIO_SEND_SAMPLE_LSB;
                when ST_AUDIO_SEND_SAMPLE_LSB =>					
                    rn.TxDataUART <= r.audio_data_in(7 downto 0); 
                    rn.state <= SEND_UART;
                    rn.stateRet <= ST_AUDIO_SAMPLE_NOW;
                    rn.audio_cont_samples <= r.audio_cont_samples + 1;
                ---------------------------------------------
                -- STATE Samplea salida de AFE
                --
                --
                ---------------------------------------------
                when ST_AFE_READ_N_SAMPLES_MSB =>
                    rn.audio_n_samples(15 downto 8) <= unsigned(r.RxDataUART);
                    -- Siguiente estado
                    rn.state <= GET_UART;
                    rn.stateRet <= ST_AFE_READ_N_SAMPLES_LSB;	
                when ST_AFE_READ_N_SAMPLES_LSB =>
                    rn.audio_n_samples(7 downto 0) <= unsigned(r.RxDataUART);
                    -- Siguiente estado
                    rn.state <= ST_AFE_SAMPLE_NOW;				
                when ST_AFE_SAMPLE_NOW =>
                    if (afe_o_out_enable='1') then	
                        rn.afe_cont_n <= 1;	-- point to first component		
                        rn.state <= ST_AFE_SEND_N_SAMPLES;
                    end if;
                    
                    if (r.audio_cont_samples >= r.audio_n_samples) then
                        rn.state <= ST_IDDLE;
                    end if;
                when ST_AFE_SEND_N_SAMPLES =>					
                    rn.TxDataUART <= std_logic_vector(afe_o_weights(r.afe_cont_n)(RC_PRECISION downto 1)); 
                    rn.afe_cont_n <= r.afe_cont_n + 1;
                    rn.state <= SEND_UART;
                    if (r.afe_cont_n = (N + 1)) then
                        rn.stateRet <= ST_AFE_SAMPLE_NOW;
                        rn.audio_cont_samples <= r.audio_cont_samples + 1;
                    else
                        rn.stateRet <= ST_AFE_SEND_N_SAMPLES;
                    end if;
                ---------------------------------------------
                -- STATE Test de AFE
                --
                --
                ---------------------------------------------
                when ST_AFE_TEST_N_SAMPLES_MSB =>
                    rn.audio_n_samples(15 downto 8) <= unsigned(r.RxDataUART);
                    rn.state <= GET_UART;
                    rn.stateRet <= ST_AFE_TEST_N_SAMPLES_LSB;	
                when ST_AFE_TEST_N_SAMPLES_LSB =>
                    rn.audio_n_samples(7 downto 0) <= unsigned(r.RxDataUART);
                    rn.state <= ST_AFE_TEST_READ_CSV;				
                when ST_AFE_TEST_READ_CSV =>                    
                    rn.state <= GET_UART;
                    rn.stateRet <= ST_AFE_TEST_SEND_CSV;

                    if (afe_o_out_enable='1') then	
                        rn.afe_cont_n <= 1;	-- point to first component		
                        rn.state <= ST_AFE_TEST_SEND_N_SAMPLES;
                    end if;
                    
                    if (r.audio_cont_samples >= r.audio_n_samples) then
                        rn.state <= ST_IDDLE;
                    end if;
                when ST_AFE_TEST_SEND_N_SAMPLES =>					
                    rn.TxDataUART <= std_logic_vector(afe_o_weights(r.afe_cont_n)(RC_PRECISION downto 1)); 
                    rn.afe_cont_n <= r.afe_cont_n + 1;
                    rn.state <= SEND_UART;
                    if (r.afe_cont_n = (N + 1)) then
                        rn.stateRet <= ST_AFE_TEST_READ_CSV;
                        rn.audio_cont_samples <= r.audio_cont_samples + 1;
                    else
                        rn.stateRet <= ST_AFE_TEST_SEND_N_SAMPLES;
                    end if;
                when ST_AFE_TEST_SEND_CSV =>					
                    afe_i_senyal_u <= signed(r.RxDataUART);
                    rn.afe_i_enable <= '1';
                    rn.state <= ST_AFE_TEST_READ_CSV;
                -----------------------------------------
                --
                -- Funciones de UART LOW LEVEL
                --
                --
                ------------------------------------------
                --
                -- Get UART 1 byte, espera indefinidamente
                -------------------------------
                when GET_UART =>					
                    -- espera FIFO UART 
                    if UART_o.RxEmpty = '0' then
                        rn.readStrobe<='1';					
                        --Espera un tiempo
                        rn.numCyclesWait <= 2;
                        rn.state <= WAIT_CYCLES;
                        rn.stateRet_WC <= ST_SEND_ECO;									
                    end if;	
                when ST_SEND_ECO =>	
                    -- Guarda en registro
                    rn.RxDataUART <= UART_o.RxData;	
                    if (r.send_echo = '1') then								
                        rn.TxDataUART <= UART_o.RxData;
                        rn.state <= SEND_UART;
                    else
                        rn.state <= r.stateRet;
                    end if;

                -------------------------------
                -- envia por UART
                -------------------------------
                when SEND_UART =>	
                    rn.TxStrobe <= '1';
                    --Espera un tiempo
                    rn.numCyclesWait <= CYCLES_1_BYTE_UART;
                    rn.state <= WAIT_CYCLES;
                    rn.stateRet_WC <= r.stateRet;
                ------------------------------
                -- Estado de esperar un ciclo de reloj
                -- vuelve al estado que tenga el registro
                -- r.stateRet_WC 
                ------------------------------
                when WAIT_CYCLES =>			
                    --Decrementa
                    rn.numCyclesWait<=r.numCyclesWait-1;
                    if(r.numCyclesWait=1)then
                        rn.state <= r.stateRet_WC ;
                    end if;	
                when others =>
                    rn.state <= ST_IDDLE;
            end case;
        end process;
            
end rtl;

