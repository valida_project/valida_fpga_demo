`timescale 1ns/10ps

/*------------------------------------------------------------------------------------------------------------
Modulo para leer por i2s microfono. Codigo original: Lattice company.

Documentacion : CCFF, Universitat Illes Ballears (2021)


@description:
	Lee por i2s los datos de microfono. 
@params:

	-DIV 	: 	Cada DIV clk_cycles samplea un bit de i2s. Son 64 bits de datos.
				Luego despues de 64*DIV clk_cycles tenemos una muestra del microfono.
				Para calcular DIV:
					DIV = clk_frec/(SR*64), donde SR es el sample rate.

				ej:
					clk_frec	SR		DIV
					-----------------------------
					12MHz		8000	23
					24MHz		8000	47

	- i_gain	:	Ganancia del microfono. Tomamos solo algunos bits para formar nuestra
				salida de 16 bits a partir de la señal original capturada de audio que
				es de 18 bits.

				GAN		output_data
				----------------------
				0		audio[17:3]
				2		audio[16:1]
				4*		audio[15:0]
				8		audio[14:0] & '0'
				16		audio[13:0] & '00'
				---------------------
				* El recomendado
				

@ports:
	Other RTL modules Interface
	--------------------------
	- [15:0] o_ld : salida original(sin aplicar offset_dc, ver port "o_ld_with_dc") de audio izquierdo en 2'complement

	- [15:0] o_rd : salida original(sin aplicar offset_dc, ver port "i_dc_value") de audio derecho en 2'complement

	- o_smp_we : salida indicando a '1' durante 1 clock cycle
				 si tiene una nueva muestra.

	User interface
	---------------------------
	- [15:0] i_dc_value	: valor de offset en dc que queremos agregarle al audio_original
						  en caso que queramos poner la se�al sobre 0

	- [15:0] i_level_th : Nivel de threshold para el cual queremos que se active la 
						  senal "o_active". Ver "o_active" description. Sirve para ver en un LED
						  por ejemplo si hay detecci�n de audio.

	- o_active: Esta senal se activa a '1' cada vez que el nivel
				de audio excede el nivel de threshold "i_level_th". Su maxima tasa de cambio
				es el sample-rate. La senal que se compara con threshold es la "o_ld_with_dc"
				
	- o_ld_with_dc : Salida de Audio con offset aplicado. Basicamente es :
						o_ld_with_dc = o_ld - i_dc_value



	I2S interface:
	----------------
	- o_bclk: 	i2s clock output. Señal que oscila con un periodo de DIV clk_cycles.
					  		 	________  							  
				o_bclk  ________		
						<-------------->
							 DIV

	- i_sd:		i2s input data.

	- o_ws:		i2s word select wire. También llamado left/right channel. El estandard dice que
				en nivel '0' lee el canal izquierdo y en '1' el derecho. Esta DIV*32 cycles en '0'
				y DIV*32 cycles en '1'


@template en verilog:
---------------------------
   // MIC driver I2S
   i2sm_in #(.DIV(23))
   u_i2sm_in ( 
      .clk(clk),
      .resetn(rst_n),
      .o_ld (audio_o_ld),
      .o_rd (audio_o_rd),
      .o_smp_we (audio_o_smp_we),
      .i_dc_value (audio_i_dc_value), 
      .i_level_th (audio_i_level_th),        
      .o_active (audio_o_active), 	
      .o_ld_with_dc (audio_o_ld_with_dc),
	  .i_gain
      .o_bclk (i2s_clk), 	
      .i_sd (i2s_dout),	
      .o_ws (i2s_ws),
	);


@template en VHDL:
-------------------------------
	I2S_mic_inst : entity work.i2sm_in
    generic map(GAIN=>4, DIV=>23)
	port map (	
	  clk,
      resetn,
      audio_o_ld,
      audio_o_rd,
      audio_o_smp_we,
      audio_i_dc_value, 
      audio_i_level_th,        
      audio_o_active, 	
      audio_o_ld_with_dc,
	  audio_i_gain	,
      i2s_clk, 	
      i2s_dout,	
      i2s_ws
	);

------------------------------------------------------------------------------------------------------------*/

module i2sm_in (
	input           	clk         , 
	input				resetn      , 
	output reg	[15:0]	o_ld        ,	
	output reg	[15:0]	o_rd        ,   
	output reg      	o_smp_we    ,   

	input 		[15:0]	i_dc_value  ,
	input 		[15:0]	i_level_th  ,

	output reg			o_active    ,
	output 		[15:0]	o_ld_with_dc,
	input 		[4:0]	i_gain ,

	output reg      	o_bclk      ,
	input           	i_sd        ,
	output          	o_ws 
);
//----------------------------
// Parameters
//----------------------------
parameter DIV     = 98;
// parameter GAIN    = 4;
parameter EN_DIFF = 0;

//----------------------------
// Signals
//----------------------------
reg	[7:0]	clk_cnt;
reg	[5:0]	bit_cnt;
reg	[63:0]	shift_reg;

wire	[17:0]	w_l;
wire	[17:0]	w_r;

always @(posedge clk or negedge resetn)
begin
    if(resetn == 1'b0)
	clk_cnt <= 8'b0;
    else if(clk_cnt == DIV)
	clk_cnt <= 8'b0;
    else 
	clk_cnt <= clk_cnt + 8'd1;
end

always @(posedge clk or negedge resetn)
begin
    if(resetn == 1'b0)
	bit_cnt <= 6'b0;
    else if(clk_cnt == DIV)
	bit_cnt <= bit_cnt + 6'd1;
end

assign o_ws = bit_cnt[5];

always @(posedge clk or negedge resetn)
begin
    if(resetn == 1'b0)
	shift_reg <= 64'b0;
    else if(clk_cnt == (DIV/2))
	shift_reg <= {shift_reg[62:0], i_sd};
end

generate if(EN_DIFF == 1)
	begin: g_on_en_diff
		reg	[2:0]	latch_d;

		reg	[17:0]	r_l_lat;
		reg	[17:0]	r_r_lat;

		reg	[17:0]	r_l_pre;
		reg	[17:0]	r_r_pre;

		always @(posedge clk or negedge resetn)
		begin
		if(resetn == 1'b0)
			latch_d <= 3'b0;
		else
			latch_d <= {latch_d[1:0], (clk_cnt == (DIV/2))};
		end

		always @(posedge clk or negedge resetn)
		begin
		if(resetn == 1'b0) begin
			r_l_lat <= 18'b0;
			r_r_lat <= 18'b0;
			r_l_pre <= 18'b0;
			r_r_pre <= 18'b0;
		end else if(latch_d[0] && (bit_cnt == 6'd0)) begin
			r_l_lat <= shift_reg[62:45] - r_l_pre ;
			r_r_lat <= shift_reg[30:13] - r_r_pre ;
			r_l_pre <= shift_reg[62:45];
			r_r_pre <= shift_reg[30:13];
		end
		end

		assign w_l = r_l_lat;
		assign w_r = r_r_lat;
	end
	else begin
		assign w_l = shift_reg[62:45];
		assign w_r = shift_reg[30:13];
	end
endgenerate

always @(posedge clk or negedge resetn)
begin
    if(resetn == 1'b0) begin
	o_ld <= 16'b0;
	o_rd <= 16'b0;
    end else if((clk_cnt == DIV) && (bit_cnt == 6'd63)) begin
	if(i_gain == 2) begin
	    o_ld <= (w_l[17] == w_l[16]) ? w_l[16:1] : {w_l[17], {15{!w_l[17]}}} ;
	    o_rd <= (w_r[17] == w_r[16]) ? w_r[16:1] : {w_r[17], {15{!w_r[17]}}} ;
	end else if(i_gain == 16) begin
	    o_ld <= ({4{w_l[17]}} == w_l[16:13]) ? {w_l[13:0],2'b0} : {w_l[17], {15{!w_l[17]}}} ;
	    o_rd <= ({4{w_r[17]}} == w_r[16:13]) ? {w_r[13:0],2'b0} : {w_r[17], {15{!w_r[17]}}} ;
	end else if(i_gain == 8) begin
	    o_ld <= ({3{w_l[17]}} == w_l[16:14]) ? {w_l[14:0],1'b0} : {w_l[17], {15{!w_l[17]}}} ;
	    o_rd <= ({3{w_r[17]}} == w_r[16:14]) ? {w_r[14:0],1'b0} : {w_r[17], {15{!w_r[17]}}} ;
	end else if(i_gain == 4) begin
	    o_ld <= ({2{w_l[17]}} == w_l[16:15]) ? w_l[15:0] : {w_l[17], {15{!w_l[17]}}} ;
	    o_rd <= ({2{w_r[17]}} == w_r[16:15]) ? w_r[15:0] : {w_r[17], {15{!w_r[17]}}} ;
	end else if(i_gain == 0) begin // half
	    o_ld <= {w_l[17], w_l[17:3]};
	    o_rd <= {w_r[17], w_r[17:3]};
	end else begin
	    o_ld <= w_l[17:2];
	    o_rd <= w_r[17:2];
	end
    end
end

always @(posedge clk or negedge resetn)
begin
    if(resetn == 1'b0)
	o_smp_we <= 1'b0;
    else 
	o_smp_we <= ((clk_cnt == DIV) && (bit_cnt == 6'd63));
end

wire	[15:0]	ld_rmdc;

assign ld_rmdc = o_ld - i_dc_value;

always @(posedge clk or negedge resetn)
begin
    if(resetn == 1'b0)
	o_active <= 1'b0;
    else 
	o_active <= ((!ld_rmdc[15]) && (ld_rmdc > i_level_th));
end

always @(posedge clk or negedge resetn)
begin
    if(resetn == 1'b0)
	o_bclk <= 1'b0;
    else 
	o_bclk <= (clk_cnt >= (DIV/2));
end

assign o_ld_with_dc = ld_rmdc;

endmodule