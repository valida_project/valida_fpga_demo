--========================================================================================================================
-- Design Unit: SOUND RECOGNITION SYSTEM BASED ON RESERVOIR COMPUTING
-- Purpose: ENDURA PROJECT 2 
-- clk Source: 50MHz 
-- Authors: J.L.Rossello (19/12/2019)
------------------------------------------------------------------------------------------------------------------------------
-- Version		Authors				Date				Changes
-- 0.1			J.L. Rossello  	07/05/2019    	First 16bits Version
-- 0.2			J.L. Rossello  	14/06/2019     Differentiate RC_PRECISION=8b (Reservoir) and BIT_PRECISION for output layer
-- 0.3			J.L. Rossello  	19/06/2019		Automatic generation
-- 1.0			J.L. Rossello 		19/12/2019		Adaptacio a la topologia de 64 entrades i 192 neurones
-- 1.1			CCFF					28/12/2019		Correccion de bugs en calculo de salidas.
-- 1.2			CCFF					30/12/2019		MODULAR
-- 1.3 			J.L. Rossello  	28/12/2022		Adaptation when the adaptive AFE output is used as input
-- 1.4			J.L. Rossello		20/05/2023		Use of 9 bits as inputs
--========================================================================================================================

LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
--USE IEEE.STD_LOGIC_SIGNED.ALL;
USE IEEE.NUMERIC_STD.ALL;

USE work.mytypes.all; --Auxiliar file for configuration purporses

ENTITY Reservoir IS
PORT ( 
		clk, reset,enable: IN STD_LOGIC; -- Reset active low, enable active high 
		input: IN unarray_simple_plus(MINPUTS DOWNTO 1);
		alpha_exp : in integer range 0 to 7;
		signo : in STD_LOGIC_VECTOR(NEURONS DOWNTO 1);
		r_exp : in integer range 0 to 7;
		x_rc : OUT unarray_simple_plus(NEURONS DOWNTO 1));
		
END ENTITY Reservoir;

ARCHITECTURE net OF reservoir IS


	COMPONENT ffD IS
	PORT ( 		
			clk, i_nreset,i_enable: IN STD_LOGIC; 		
			i_senyal_u: IN SIGNED ((RC_PRECISION) DOWNTO 0); 
			o_qout: OUT SIGNED ((RC_PRECISION) DOWNTO 0));
	END COMPONENT;

	COMPONENT act_fct IS
	PORT ( i_x: IN SIGNED (RC_PRECISION+1 DOWNTO 0);
			o_f: OUT SIGNED ((RC_PRECISION-1) DOWNTO 0));
	END COMPONENT;
	SIGNAL out_x_aux : UNARRAY_SIMPLE(NEURONS DOWNTO 1);
	SIGNAL prod1,prod2,out_x,out_x_ant : UNARRAY_SIMPLE_PLUS(NEURONS DOWNTO 1);
	SIGNAL sum: unarray_plus2(NEURONS DOWNTO 1);
--	SIGNAL input: UNARRAY_PLUS(MINPUTS DOWNTO 1);
	
BEGIN

	--signo<=X"f09b65f976494ad9ccc8af28187765d8ec3bf461ed1a4487";--Reservoir connectivity code
--	geninputs: FOR i IN 1 TO MINPUTS GENERATE  -- LLEVAM ES BIAS
--		input(i)<=resize(entrada(i),RC_PRECISION+1)+to_signed(BIAS,RC_PRECISION+1); -- Add bias to inputs. 
--	END GENERATE;

	-- 1st, 2nd & 3rd neuron 
	
	-- External products
	prod1(1) <= input(1) when signo(1)='1' else -input(1);
	prod1(2) <= input(1) when signo(2)='1' else -input(1);
	prod1(3) <= input(1) when signo(3)='1' else -input(1);

	-- Internal product
	prod2(1) <= resize(out_x(NEURONS),prod2(1)'length)-resize(shift_right(out_x(NEURONS), r_exp),prod2(1)'length); -- Internal hyperparameter fixed to r=0.9375 in this line
	prod2(2) <= resize(out_x(1),prod2(2)'length)-resize(shift_right(out_x(1), r_exp),prod2(2)'length); -- Internal hyperparameter fixed to r=0.9375 in this line
	prod2(3) <= resize(out_x(2),prod2(3)'length)-resize(shift_right(out_x(2), r_exp),prod2(3)'length); -- Internal hyperparameter fixed to r=0.9375 in this line

	-- Sum of the previous terms. Increase one bit to avoid overflow
	sum(1) <= resize(prod1(1),RC_PRECISION+2)+resize(prod2(1),RC_PRECISION+2); -- array ‘sum’ must be resized to RC_PRECISION+2 to ensure no-overflows
	sum(2) <= resize(prod1(2),RC_PRECISION+2)+resize(prod2(2),RC_PRECISION+2); 
	sum(3) <= resize(prod1(3),RC_PRECISION+2)+resize(prod2(3),RC_PRECISION+2);

	f_tanh1: act_fct PORT MAP(sum(1),out_x_aux(1)); -- put the result to the piece-wise activation function act_fct, you obtain out_x_aux
	f_tanh2: act_fct PORT MAP(sum(2),out_x_aux(2)); -- put the result to the piece-wise activation function act_fct, you obtain out_x_aux
	f_tanh3: act_fct PORT MAP(sum(3),out_x_aux(3)); -- put the result to the piece-wise activation function act_fct, you obtain out_x_aux


	out_x_ant(1)<= 	resize( out_x(1) + shift_right(resize(out_x_aux(1), out_x(1)'length) - 
							out_x(1) ,alpha_exp), out_x_ant(1)'length);				
	out_x_ant(2)<= 	resize( out_x(2) + shift_right(resize(out_x_aux(2), out_x(2)'length) - 
							out_x(2) ,alpha_exp), out_x_ant(2)'length);				
	out_x_ant(3)<= 	resize( out_x(3) + shift_right(resize(out_x_aux(3), out_x(3)'length) - 
							out_x(3) ,alpha_exp), out_x_ant(3)'length);				
						

	ff1: ffD PORT MAP(clk, reset, enable, out_x_ant(1),out_x(1)); -- Force a delay of one clk cycle to each neuron
	ff2: ffD PORT MAP(clk, reset, enable, out_x_ant(2),out_x(2)); -- Force a delay of one clk cycle to each neuron
	ff3: ffD PORT MAP(clk, reset, enable, out_x_ant(3),out_x(3)); -- Force a delay of one clk cycle to each neuron

	-- Rest of the reservoir (MINPUTS-1 groups of three neurons)

	reservorio: FOR i IN 2 TO MINPUTS GENERATE
		-- External products
		prod1((i-1)*3+1) <= input(i) when signo((i-1)*3+1)='1' else -input(i);
		prod1((i-1)*3+2) <= input(i) when signo((i-1)*3+2)='1' else -input(i);
		prod1((i-1)*3+3) <= input(i) when signo((i-1)*3+3)='1' else -input(i);

		-- Internal product
		prod2((i-1)*3+1) <= resize(out_x((i-1)*3+0),prod2(1)'length)-resize(shift_right(out_x((i-1)*3+0), r_exp),prod2(1)'length); -- Internal hyperparameter fixed to r=0.9375 in this line
		prod2((i-1)*3+2) <= resize(out_x((i-1)*3+1),prod2(2)'length)-resize(shift_right(out_x((i-1)*3+1), r_exp),prod2(2)'length); -- Internal hyperparameter fixed to r=0.9375 in this line
		prod2((i-1)*3+3) <= resize(out_x((i-1)*3+2),prod2(3)'length)-resize(shift_right(out_x((i-1)*3+2), r_exp),prod2(3)'length); -- Internal hyperparameter fixed to r=0.9375 in this line
		
		-- Sum of the previous terms. Increase one bit to avoid overflow
		sum((i-1)*3+1) <= resize(prod1((i-1)*3+1),RC_PRECISION+2)+resize(prod2((i-1)*3+1),RC_PRECISION+2); 
		sum((i-1)*3+2) <= resize(prod1((i-1)*3+2),RC_PRECISION+2)+resize(prod2((i-1)*3+2),RC_PRECISION+2); 
		sum((i-1)*3+3) <= resize(prod1((i-1)*3+3),RC_PRECISION+2)+resize(prod2((i-1)*3+3),RC_PRECISION+2); 		
		
		f_tanh1x: act_fct PORT MAP(sum((i-1)*3+1),out_x_aux((i-1)*3+1)); -- put the result to the piece-wise activation function act_fct, you obtain out_x_aux
		f_tanh2x: act_fct PORT MAP(sum((i-1)*3+2),out_x_aux((i-1)*3+2)); -- put the result to the piece-wise activation function act_fct, you obtain out_x_aux
		f_tanh3x: act_fct PORT MAP(sum((i-1)*3+3),out_x_aux((i-1)*3+3)); -- put the result to the piece-wise activation function act_fct, you obtain out_x_aux

		
	out_x_ant((i-1)*3+1)<= 	resize( out_x((i-1)*3+1) + shift_right(resize(out_x_aux((i-1)*3+1), out_x((i-1)*3+1)'length) - 
							out_x((i-1)*3+1) ,alpha_exp), out_x_ant((i-1)*3+1)'length);				
	out_x_ant((i-1)*3+2)<= 	resize( out_x((i-1)*3+2) + shift_right(resize(out_x_aux((i-1)*3+2), out_x((i-1)*3+2)'length) - 
							out_x((i-1)*3+2) ,alpha_exp), out_x_ant((i-1)*3+2)'length);				
	out_x_ant((i-1)*3+3)<= 	resize( out_x((i-1)*3+3) + shift_right(resize(out_x_aux((i-1)*3+3), out_x((i-1)*3+3)'length) - 
							out_x((i-1)*3+3) ,alpha_exp), out_x_ant((i-1)*3+3)'length);				

		ff1: ffD PORT MAP(clk, reset, enable, out_x_ant((i-1)*3+1),out_x((i-1)*3+1)); -- Force a delay of one clk cycle to each neuron
		ff2: ffD PORT MAP(clk, reset, enable, out_x_ant((i-1)*3+2),out_x((i-1)*3+2)); -- Force a delay of one clk cycle to each neuron
		ff3: ffD PORT MAP(clk, reset, enable, out_x_ant((i-1)*3+3),out_x((i-1)*3+3)); -- Force a delay of one clk cycle to each neuron

	END GENERATE;

	x_rc<=out_x;
--	x_rc<=prod2;
END net;
