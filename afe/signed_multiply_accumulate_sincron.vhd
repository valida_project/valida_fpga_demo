library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
USE work.mytypes.all;

entity signed_multiply_accumulate_sincron is

	port 
	(
		clk		   : in std_logic;
		reset	   : in std_logic;
		a		   : in signed((BIT_PRECISION-1) downto 0);
		b		   : in signed (RC_PRECISION downto 0);
		bias		: in signed ((2*BIT_PRECISION-1) downto 0);
		accum_out    : out signed ((2*BIT_PRECISION-1) downto 0)
	);

end entity;

architecture rtl of signed_multiply_accumulate_sincron is

	-- Declare registers for intermediate values
	signal mult_reg : signed ((2*BIT_PRECISION-1) downto 0);
	signal adder_out : signed ((2*BIT_PRECISION-1) downto 0);
	signal old_result : signed ((2*BIT_PRECISION-1) downto 0);

begin
	mult_reg<=a*resize(b,BIT_PRECISION); -- Signal 'b' resized bo BIT_PRECISION
	old_result <= bias when reset='0' else adder_out;
	
	process (clk)
	begin
		if (falling_edge(clk)) then
			if (reset='0') then
				accum_out<=adder_out;  	
			end if;
			-- Store accumulation result in a register
				adder_out <= old_result + mult_reg;
		end if;
	end process;

end rtl;
