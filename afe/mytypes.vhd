-- Josep L. Rossello. Universitat Illes Balears. Dec 2019. Modified Ago 2022

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
use ieee.numeric_std.all;

package mytypes is

-- Definitions for the reservoir topology
constant RC_PRECISION: integer:=8; -- Precision of the reservoir
constant BIT_PRECISION: integer:=16; -- Precision of the weights

--Definitions for the adaptive audio preprocessing block
constant N: integer:=63; -- Number of neurons in the adaptive platform. Here, the number of inputs is 1 (the input audio signal)
constant L: integer:=128; -- Sample length in the adaptive platform.
--constant MAC_THRESHOLD_1 : INTEGER RANGE 0 TO 32767 := 12000; --Threshold used in the histeresis comparator block
--constant MAC_THRESHOLD_2 : INTEGER RANGE 0 TO 32767 := 8000; --Threshold used in the histeresis comparator block


--EMA integrator
constant N_INTEGRATION: integer:=7; -- Related to the 'r' value of the EMA inegrator (r=1-1/2^N_INTEGRATION)
constant PRECISION_COMPARE: integer:=11; -- Best theoretical is 12
constant NEURONS: integer:=192; --Number of neurons.
constant MINPUTS: integer:=64; -- Number of inputs. This is also the number of neurons in the reservoir of the adaptive system (N)

--constant FINGERPRINT_BITS: integer:=256; -- NEURONS+MINPUTS
constant FINGERPRINT_ARRAY: integer:=96; -- ONLY INPUTS

constant ALPHA_EXP: integer:=3; -- alpha=1-1/2^ALPHA_EXP 

--Noise Avoidance Block parameters
--constant N_NOISE: integer:=8; -- Number of samples taken in the noise-avoidance block
--constant Td : integer:=10; -- Sampling time for the noise-avoidance block
--constant Tsamples : integer:=13; -- Integration time for fingerprint samples

--ACTIVAR TRAINING_UTTERANCES EN MODELO ARRIA!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--constant TRAINING_UTTERANCES : integer:=2; -- number of utterances we want for the training
--constant LOG2_TRAINING_UTTERANCES : integer:=1; --log2 of TRAINING_UTTERANCES
CONSTANT TRAINING_UTTERANCES : INTEGER RANGE 0 TO 2 := 2;
CONSTANT LOG2_TRAINING_UTTERANCES : INTEGER RANGE 0 TO 1 :=1;
			
--Parameter for the controlling block (UPDATING sensitivity of the reference EMA vector during normal mode)
constant UPDATE : integer:=2;
constant THRESHOLD1: integer:=240; -- bias applied to inputs for the reservoir processing -- LINE ACTIVATED FOR THE PETRA DEV. BOARD PHASE
type TH_TYPE is ARRAY(0 TO 7) of integer range 0 to 255;
constant THRESHOLD_VECTOR: TH_TYPE:=(85,80,75,60,50,50,50,50);
--Parameter for the stochastic comparator block

--ACTIVAR THRESHOLD EN MODELO ARRIA!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--constant THRESHOLD: integer:=20; --Threshold for identifying the speaker (25 for "sheila")


-- Definition of arrays with arbitrary length of BIT_PRECISSION, BIT_PRECISSION+1, and double precission (unarray, unarray_plus and unarray_double)
type unarray_simple IS ARRAY (NATURAL RANGE <>) OF SIGNED (RC_PRECISION-1 DOWNTO 0);

type unarray_simple_plus IS ARRAY (NATURAL RANGE <>) OF SIGNED (RC_PRECISION DOWNTO 0);
type unarray_simple_plus2 IS ARRAY (NATURAL RANGE <>) OF SIGNED (RC_PRECISION+1 DOWNTO 0);
type unarray_simple_unsigned IS ARRAY (NATURAL RANGE <>) OF UNSIGNED (RC_PRECISION-1 DOWNTO 0);
type unarray_overflow_unsigned IS ARRAY (NATURAL RANGE <>) OF UNSIGNED (BIT_PRECISION-1 DOWNTO PRECISION_COMPARE+1);
type unarray IS ARRAY (NATURAL RANGE <>) OF SIGNED (BIT_PRECISION-1 DOWNTO 0);
type unarray_plus IS ARRAY (NATURAL RANGE <>) OF SIGNED (RC_PRECISION DOWNTO 0);
type unarray_plus2 IS ARRAY (NATURAL RANGE <>) OF SIGNED (RC_PRECISION+1 DOWNTO 0);
type UNARRAY_DOUBLE IS ARRAY (NATURAL RANGE <>) OF SIGNED ((2*BIT_PRECISION-1) DOWNTO 0);
type matrix  is array (natural range <>, natural range <>) of SIGNED(RC_PRECISION-1 downto 0); -- Case of a matrix
--type stochastic_array IS ARRAY (NATURAL RANGE <>) OF STD_LOGIC_VECTOR (N_NOISE-1 DOWNTO 0);
--type unsigned_array IS ARRAY (NATURAL RANGE <>) OF unsigned (N_NOISE-1 DOWNTO 0);
type unarray_8 IS ARRAY (NATURAL RANGE <>) OF STD_LOGIC_VECTOR (7 DOWNTO 0);
type State_type IS (beginning_mode,training_mode,transitory_mode,normal_mode);
type State_type2 IS (detection,non_detection);
end mytypes;

