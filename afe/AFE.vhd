-- Design Unit: ADAPTIVE FEATURE EXTRACTION BLOCK
-- Purpose: VALENTRA AUDIO PROJECT
-- Clock Source: 50MHz 
-- Authors: J.L.Rossello (15/07/2021)
--------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Version		Authors				Date				Changes
-- 0.1			J.L. Rossello  		15/07/2021    			First Version
-- 0.2 			J.L. Rossello 			22/07/2022			Inclusion of output enable
-- 0.3			C. Franco Frasser		July/2022			Debug of code
-- 0.4			J.L. Rossello			17/08/2022			Adaptation to rest of the Speaker recognition: Change N to MINPUTS-1, 				
--											Reduce dimension of o_weights to RC_PRECISION instead of RC_PRECISION+1
--==================================================================================================================================================================

LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_SIGNED.ALL;
USE IEEE.NUMERIC_STD.ALL;
USE WORK.MYTYPES.ALL; --Auxiliar file for configuration purporses

ENTITY AFE IS
	PORT
	(
		clk		  : IN STD_LOGIC;
		i_nreset	  : IN STD_LOGIC;
		i_enable	  : IN STD_LOGIC;
		i_senyal_u	: IN SIGNED((RC_PRECISION-1) DOWNTO 0); --Input audio signal to process
		o_out_enable : OUT STD_LOGIC;
		o_weights : OUT UNARRAY_SIMPLE_PLUS(MINPUTS DOWNTO 1)-- post-processed audio signal to multidimensional adaptive output 

	);	
END ENTITY;

ARCHITECTURE structural OF AFE IS

COMPONENT reservoir_AFE IS
PORT ( 
		clk, i_nreset,i_enable: IN STD_LOGIC;
		i_senyal_u: IN SIGNED ((RC_PRECISION-1) DOWNTO 0);
		o_x_rc : OUT UNARRAY_SIMPLE(MINPUTS-1 DOWNTO 1));
END COMPONENT;

SIGNAL entrada :  UNARRAY_SIMPLE(0 TO 31);
SIGNAL y_reservoir :  UNARRAY_SIMPLE(MINPUTS-1 DOWNTO 1);
SIGNAL zc,zu,wpre : unarray_plus(MINPUTS DOWNTO 1);
SIGNAL rco : STD_LOGIC;
SIGNAL cnt : INTEGER RANGE 0 TO L-1;


begin

	-- A 31 Neurons reservoir
	Resevoir_31b: reservoir_AFE PORT MAP(clk, i_nreset, i_enable, i_senyal_u,y_reservoir); -- Force a delay of one clock cycle to each neuron

	-- Delaying 'tau=1' the reservoir output & join it to input signal
	PROCESS (clk)
	BEGIN
		IF (rising_edge(clk)) THEN
			IF (i_enable='1') THEN
				FOR i IN 1 TO MINPUTS-1 LOOP 
					zc(i) <= - resize(y_reservoir(i),RC_PRECISION+1);
				END LOOP;
				zc(MINPUTS ) <= - resize(i_senyal_u,RC_PRECISION+1);
			END IF;
		END IF;
	END PROCESS;
	
	evaluador_w: FOR i IN 1 TO MINPUTS GENERATE
		zu(i)<=zc(i)+i_senyal_u;
	END GENERATE;

	--Evaluation of the max-plus pseudo-inverse
	PROCESS (clk)
	BEGIN
		IF (rising_edge(clk)) THEN
			IF (i_enable='1') THEN
				IF (rco = '1') THEN
					wpre<=zu;
					-- o_weights<=resize(wpre,RC_PRECISION); -- WE MUST REDUCE THE RESULT OF THE ADAPTIVE SYSTEM TO ITS 8 MSBITS
					-- o_weights<=wpre(MINPUTS DOWNTO 1)(RC_PRECISION DOWNTO 1);-- WE MUST REDUCE THE RESULT OF THE ADAPTIVE SYSTEM TO 8 MSBITS
					FOR i IN 1 TO MINPUTS LOOP --wpre'RANGE LOOP
--						o_weights(i)<=wpre(i)(RC_PRECISION DOWNTO 1);-- WE MUST REDUCE THE RESULT OF THE ADAPTIVE SYSTEM TO 8 MSBITS
						o_weights(i)<=wpre(i);
					END LOOP;

					--o_weights<=wpre;
				ELSE 
					FOR i IN 1 TO MINPUTS  LOOP 
						IF (zu(i)<wpre(i)) THEN
							wpre(i)<=zu(i);
						END IF;
					END LOOP;
				END IF;
			END IF;
		END IF;
	END PROCESS;

	-- Counter to generate rco (active each L inputs)
	PROCESS (clk)
	BEGIN
		IF (rising_edge(clk)) THEN

			IF i_nreset = '0' THEN
				-- Reset the counter to 0
				cnt <= 0;

			ELSIF i_enable = '1' THEN
				-- Increment the counter if counting is enabled			   
				cnt <= cnt + 1;
				
				IF (cnt=L-1) THEN
					rco<='1';
					cnt<=0;
				ELSE 
					rco<='0';
				END IF;

			END IF;
		END IF;
		--qs<=to_signed(cnt,qs'length);
		-- Output the current count
	END PROCESS;
	o_out_enable<=rco;
	
END structural;
