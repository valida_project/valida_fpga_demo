--========================================================================================================================
-- Design Unit: Speaker recognitio project Agost 2022
-- Purpose: VALIDA PROJECT (PDC2021, PDC2020, VALIDA 2022, ENDURA PROJ 3). Speaker Recognition System 
-- Clock Source: 50MHz 
-- Authors: Josep L. Rossello Sanz & Christian Franco Frasser.  Palma de Mallorca, Spain
-- Electronic Engineering Group. Industrial Engineering and Construction Dept. Universitat de les Illes Balears. 
------------------------------------------------------------------------------------------------------------------------------
-- Version		Authors			Date				Changes
-- 0.0			J.L. Rossello  	26/09/2021  First 8b Version with automatic threshold selection from original ENDURA Proj. 3
-- 0.1			J.L. Rossello		17/08/2022	Adaptation to VALIDA project (w/o using MELFCC from ENDURA and incorporating Adaptive audio processing)
-- 0.2 			J.L. Rossello		09/11/2022 	Only RC system
-- 0.3			J.L. Rossello 		25/12/2022 	Debuging RC system only
-- 0.4 			J.L. Rossello 		10/01/2023	Integration of the full system
-- 0.5			J.L. Rossello		20/05/2023	Debug of AFE output (increase from 8bits to 9bits)
--========================================================================================================================

--Auxiliary libraries
LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_SIGNED.ALL;
USE IEEE.NUMERIC_STD.ALL;
USE work.mytypes.all; 
ENTITY valida IS
	PORT(	
			clk : IN STD_LOGIC; --clock signal
			ctrl_n_rst,ctrl_en,i_train : IN STD_LOGIC; -- Reset active low and Enable active high (when new audio data is available)
			i_senyal_u	: IN SIGNED((RC_PRECISION -1) DOWNTO 0); --Input audio signal to process
			
			i_exp_delta : IN INTEGER RANGE 0 TO 3;
			i_mac_thresh_high : IN INTEGER RANGE 0 TO 262143 ; --Threshold used in the histeresis comparator block
			i_mac_thresh_low : IN INTEGER RANGE 0 TO 262143 ; --Threshold used in the histeresis comparator block

			o_reservoir: OUT SIGNED(2*BIT_PRECISION-1 DOWNTO 0);
			o_y_out,o_internal_enable,o_state_bit,o_detect : OUT STD_LOGIC -- Stochastic comparator output. Output for the PETRA dev. phase
--			o_state_bit: OUT STD_LOGIC; -- Signal indicating the operation mode (0 for training mode, 1 for normal mode)
--			o_y_reservoir: OUT STD_LOGIC -- Signal indicating the presence or not of an specific keyword
	);
END valida;

ARCHITECTURE principal OF valida IS 

COMPONENT AFE IS
	PORT
	(
		clk		  : IN STD_LOGIC;
		i_nreset	  : IN STD_LOGIC;
		i_enable	  : IN STD_LOGIC;
		i_senyal_u	: IN SIGNED((RC_PRECISION -1) DOWNTO 0); --Input audio signal to process
		o_out_enable : OUT STD_LOGIC;
		o_weights : OUT unarray_simple_plus(MINPUTS DOWNTO 1)-- post-processed audio signal to multidimensional adaptive output 

	);	
END COMPONENT;


-- DEBUG SIGnals	
	SIGNAL index_aux: INTEGER RANGE 0 TO 255;
	SIGNAL out_enable,internal_enable : STD_LOGIC; --Output comming from the AFE block indicating that the output is enabled
--	--  signals from the reservoir
	SIGNAL rc_input : UNARRAY_SIMPLE_PLUS(MINPUTS-1 DOWNTO 0); 
--	-- Index for memory location
--	-- reservoir config parameters.
	SIGNAL rc_signo :  STD_LOGIC_VECTOR(NEURONS-1 DOWNTO 0);
	SIGNAL rc_r_exp : INTEGER RANGE 0 TO 7;

--	SIGNAL uk_input : UNARRAY_SIMPLE(MINPUTS-1 DOWNTO 0); -- output of the ADAPTIVE block, use only if NAB is used

	
--	-- signals for output reading
	SIGNAL rc_yout : unarray_simple_plus (NEURONS-1 DOWNTO 0);
	SIGNAL en_tick, q, internal_n_rst : STD_LOGIC;
	SIGNAL reservoir_n_reset,y_count : STD_LOGIC;
	
	--Signals for histeresis comparator
	SIGNAL y_reservoir,y_reservoir_i,y_positive,y_negative: STD_LOGIC;
	SIGNAL y_out_long,y_out: STD_LOGIC;
	SIGNAL Sdiff,Sref:STD_LOGIC_VECTOR(0 TO FINGERPRINT_ARRAY-1);
--	SIGNAL similarity: unarray_8(0 TO 7);
	SIGNAL sum,bias: SIGNED(2*BIT_PRECISION-1 DOWNTO 0);
	SIGNAL index_neuron_aux: INTEGER RANGE 0 TO NEURONS+MINPUTS-1;
	SIGNAL weights: UNARRAY(NEURONS+MINPUTS-1 DOWNTO 0);
	SIGNAL state_vector: UNARRAY_SIMPLE_PLUS(NEURONS+MINPUTS-1 DOWNTO 0);
	SIGNAL fingerprint_vector,Uacc_ref1,Uacc_ref2,Uacc_diff_ref,Uacc_delta: UNARRAY(FINGERPRINT_ARRAY-1 DOWNTO 0); -- Comment for debug
	SIGNAL Uacc_ref : UNARRAY_DOUBLE(FINGERPRINT_ARRAY-1 DOWNTO 0);
	SIGNAL Uacc_ref_normal :UNARRAY(FINGERPRINT_ARRAY-1 DOWNTO 0);
	SIGNAL Udiff : UNARRAY(FINGERPRINT_ARRAY-1 DOWNTO 0);
	SIGNAL pass_count: INTEGER RANGE 0 TO 7;
	SIGNAL state: State_type;
	SIGNAL outstate: State_type2;
	SIGNAL zero: STD_LOGIC_VECTOR(2*BIT_PRECISION-1 DOWNTO 0):=(OTHERS=>'0');
--	
--	-- PARAMETERS TO IDENTIFY NUMBER OF SHOTS FOR LEARNING (DEFAULT IS 2 SHOTS)
--
--	CONSTANT TRAINING_UTTERANCES2 : INTEGER RANGE 0 TO 2 := 2;
--	CONSTANT LOG2_TRAINING_UTTERANCES2 : INTEGER RANGE 0 TO 1 :=1;

-- DEBUGING SIGNALS
signal input_u		:  signed((RC_PRECISION -1) downto 0);
--signal rc_entrada : signed((RC_PRECISION-1) downto 0);
--signal cnt2,cnt3 : integer range 0 to 31;
--signal cnt4 : integer range 0 to NEURONS-1;


BEGIN

-- DEBUGING CODE
--input_u<=TO_SIGNED(111,RC_PRECISION);
--process (clk)
--begin
--	if (rising_edge(clk)) then
--
--		if ctrl_n_rst = '0' then
--			-- Reset the counter to 0
--			cnt4<= 0;
--
--		else
--			-- Increment the counter if counting is enabled	
--			if cnt4=NEURONS-1 then
--				cnt4<=0;
--			else
--				cnt4<=cnt4+1;
--			end if;	
--		end if;
--	end if;
--
--	-- Output the current count
--end process;
--o_reservoir<=resize(rc_yout(cnt4),o_reservoir'length);

o_reservoir<=resize(sum,o_reservoir'length);

	-- Generation of the single tick to process each new MFCC vector signal
	PROCESS(clk) --dff
	BEGIN
		IF rising_edge(clk) THEN
			q <= ctrl_en;
		END IF;
	END PROCESS;
	en_tick <= ctrl_en AND (NOT q);


----------------------------------------------------------------------------------------------------------------------------------
--			Audio Feature Extraction Block (AFE)
----------------------------------------------------------------------------------------------------------------------------------
--Internal_AFE: AFE PORT MAP(clk, ctrl_n_rst,en_tick, input_u,out_enable,rc_input); -- Force a delay of one clock cycle to each neuron	
	
	Internal_AFE: AFE PORT MAP(clk, ctrl_n_rst,en_tick, i_senyal_u,out_enable,rc_input); -- Force a delay of one clock cycle to each neuron	
	internal_enable<=out_enable AND en_tick;
	o_internal_enable<=internal_enable;
	
--	--------------------------------------------------------------------------------------------------------------------------------
--			 End of the AFE Block
--	--------------------------------------------------------------------------------------------------------------------------------
	
--	--------------------------------------------------------------------------------------------------------------------------------
--			 Reservoir block (used to obtain the correct ending of each keyword pronunciated) DEBUGED
--	--------------------------------------------------------------------------------------------------------------------------------
		rc_signo<=X"f09b65f976494ad9ccc8af28187765d8ec3bf461ed1a4487"; -- Adaptacio a la entrada projecte VALIDA (AFE adaptiu)
					   
		rc_r_exp<=4; 
--		reservoir_n_reset<=ctrl_n_rst AND (NOT y_count); -- per activar al projecte cencer !!!!!!!!!!!!!!!!!1111111
		reservoir_n_reset<=ctrl_n_rst ; -- per llevar al projecte cencer !!!!!!!!!!!!!!!!!1111111
		
		rc_inst : entity work.Reservoir    
		port map (clk, reservoir_n_reset, internal_enable,  
				  rc_input, 
				  ALPHA_EXP,  -- this line is used in the PETRA development steps
				  rc_signo,
				  rc_r_exp,
				  rc_yout
		);
		-- State vector creation
		state_vector(MINPUTS-1 DOWNTO 0)<= rc_input;
		state_vector(NEURONS+MINPUTS-1 DOWNTO MINPUTS)<= rc_yout;
		
--	----------------------------------------------------------------	----------------------------------------------------------------
--			--  END OF RESERVOIR (debuged)-----------------------
--	----------------------------------------------------------------	----------------------------------------------------------------


----- DEBUG COUNTER FOR RESERVOIR
--	process (clk) 
--	begin
--		if (rising_edge(clk)) then
--			if reservoir_n_reset = '0' then
--			-- Reset the counter to 0
--				index_aux <= 0;
--			elsif en_tick='1' then
--			-- Increment the counter 
--					index_aux <= index_aux + 1;	
--			end if;
--		end if;
--	end process;	
--	o_index<=index_aux;


--	----------------------------------------------------------------	----------------------------------------------------------------
--			--  MAC instantiation (debuged)-----------------------
--	----------------------------------------------------------------	----------------------------------------------------------------
		

	-- MAC Instantiation and generation of y_reservoir
	-- Reservoir Instantiation
	--	state_vector<=( OTHERS => to_signed(1,RC_PRECISION)); -- only for debug. not for final version
		mac_inst : entity work.signed_multiply_accumulate_sincron
		port map (clk, internal_n_rst,  
				  weights(index_neuron_aux), --weights -- Activate in the normal mode
				  --to_signed(1,BIT_PRECISION), -- only for the debug mode
				  state_vector(index_neuron_aux), -- State vector z=(u,x)
				  bias, --Bias of MAC function -- activate in the normal mode 
				  --to_signed(0,2*BIT_PRECISION),
				  sum -- Adder output
		);
		
	-- MAC timing block
--	process (clk) 
--	begin
--		if (rising_edge(clk)) then
--			if internal_enable = '1' then
--			-- Reset the counter to 0
--				index_neuron_aux <= 0;
--				internal_n_rst<='0';
--			else
--			-- Increment the counter 
--				if (index_neuron_aux=NEURONS+MINPUTS-2) then
--					index_neuron_aux <= index_neuron_aux + 1;	
----					internal_n_rst<='0';--Odrinary RCO
--					internal_n_rst<='1';--Delayed RCO
--				elsif (index_neuron_aux=NEURONS+MINPUTS-1) then
----					internal_n_rst<='1'; --Ordinary RCO
--					internal_n_rst<='0'; --Delayed RCO
--					index_neuron_aux <= 0;					
--				else 
--					internal_n_rst<='1';
--					index_neuron_aux <= index_neuron_aux + 1;										
--				end if;
--			end if;
--		end if;
--	end process;	

	
	-- MAC timing block
	process (clk) 
	begin
		if (rising_edge(clk)) then
			if internal_enable = '1' then
			-- Reset the counter to 0
				index_neuron_aux <= 0;
				internal_n_rst<='0';
			else
			-- Increment the counter 
				if (index_neuron_aux=NEURONS+MINPUTS-1) then
					internal_n_rst<='0'; --Delayed RCO
					index_neuron_aux <= 0;					
				else 
					internal_n_rst<='1';
					index_neuron_aux <= index_neuron_aux + 1;										
				end if;
			end if;
		end if;
	end process;	
	
--	----------------------------------------------------------------	----------------------------------------------------------------
--			--  END of MAC instantiation (debuged)-----------------------
--	----------------------------------------------------------------	----------------------------------------------------------------
	
	
--		 o_reservoir<=sum;  --ONLY FOR DEBUGING MODE
------------------------------------------------------------------	----------------------------------------------------------------		
		--HYSTERESIS COMPARATOR
------------------------------------------------------------------	----------------------------------------------------------------

		y_positive<='1' when sum>to_signed(-i_mac_thresh_high,sum'length) else '0'; --pst is one if sum is greater than v1=2^15
		y_negative<='1' when sum>to_signed(-i_mac_thresh_low,sum'length) else '0'; --ngt is one if sum is lower than v1=-2^15-1
--		--y_negative<='1' when sum>to_signed(-52767,sum'length) else '0'; --ngt is one if sum is lower than v1=-2^15-1
		y_reservoir_i<=y_positive when y_reservoir='0' else y_negative; -- This is the output of the reservoir indicating the presence of an utterance
		o_y_out<=y_reservoir_i; -- output of the reservoir. Only for the PETRA dev. phase --REACTIVAR DESPUES ESTA LINEA!!!!!
--
		process (clk) -- generation of y_reservoir as a delayed version of y_reservoir_i to implement hysteresis and know when the keyword ends
		begin
			if (rising_edge(clk)) then
				if (internal_enable = '1') then
					y_reservoir<=y_reservoir_i;
				end if;
			end if;
		end process;
		
		y_count<=y_reservoir	AND NOT (y_reservoir_i); --Indicates if we are just in the time at the end of a keyword

------------------------------------------------------------------	----------------------------------------------------------------		
		-- END OF HYSTERESIS COMPARATOR
------------------------------------------------------------------	----------------------------------------------------------------




------------------------------------------------------------------	----------------------------------------------------------------		
--		-- EMA INTEGRATOR BLOCK
------------------------------------------------------------------	----------------------------------------------------------------		
		
		GENERATE_EMA_INTEGRATOR: for k in 0 to FINGERPRINT_ARRAY-1 generate
			ema_inst : entity work.EMA_Integrator
			port map (clk, ctrl_n_rst, internal_enable, y_reservoir, 
					state_vector(k), -- Filtered MFCC input
            --        rc_input(k),
                    fingerprint_vector(k)
--					Uacc_k(k) -- EMA output
			);
		end generate GENERATE_EMA_INTEGRATOR;
		

------------------------------------------------------------------	----------------------------------------------------------------		
--		-- END OF THE EMA INTEGRATOR BLOCK
------------------------------------------------------------------	----------------------------------------------------------------		



	
--		------------------------------------------------------------------------------------------------------
--		-- FSM changing from normal_mode to training_mode when necessary, Controlling block-------------------
--		------------------------------------------------------------------------------------------------------

		process (clk)
		begin
			if (rising_edge(clk)) then
				if ctrl_n_rst = '0' then
					state<=beginning_mode;
					Uacc_ref<=(others => SIGNED(zero));
				elsif internal_enable = '1' and y_count='1' then
					case state is
						when beginning_mode=>
							if (i_train='1') then
								state<=training_mode;
								for k in 0 to FINGERPRINT_ARRAY-1 loop
									Uacc_ref(k)<=Uacc_ref(k)+resize(fingerprint_vector(k),2*BIT_PRECISION);
								end loop;
								Uacc_ref1<=fingerprint_vector;
							else
								Uacc_ref<=(others => SIGNED(zero));
								state<=beginning_mode;
							end if;
						when training_mode=>
							if (i_train='1') then
								state<=transitory_mode;
								for k in 0 to FINGERPRINT_ARRAY-1 loop
									Uacc_ref(k)<=Uacc_ref(k)+resize(fingerprint_vector(k),2*BIT_PRECISION);
								end loop;
								Uacc_ref2<=fingerprint_vector;
							else
								state<=training_mode;
							end if;						
						when transitory_mode=>
							for k in 0 to FINGERPRINT_ARRAY-1 loop -- divide by 4 once in normal mode (for the case of training with 2 keywords, please change this 
								Uacc_ref(k)<=shift_right(Uacc_ref(k),LOG2_TRAINING_UTTERANCES); -- if finally the number of keywords in the few-shot learning phase is increased
							end loop; 
							state <= normal_mode;							
						when normal_mode=>
							state <= normal_mode;							
					end case;
				end if;
			end if;
		end process;

		o_state_bit<='0' when state=normal_mode else '1'; --REACTIVAR DESPUES ESTA LINEA!!!!!-- State bit indicates when we are in the training step (active led at PETRA board), else it is in normal mode



--		
--		
---- **********ACTIVAR AQUESTES TRES LINIES EN CAS DE UPDATE CONTINUAT **********
----		Uref_Update: for k in 0 to FINGERPRINT_ARRAY-1 generate -- 'Ureg' is an Auxiliary signal for the updating process of Uacc_ref
----			Ureg(k)<=resize(Uacc_k(k),2*BIT_PRECISION)-Uacc_ref(k);
----		end generate Uref_Update;
---- **********ACTIVAR AQUESTES TRES LINIES EN CAS DE UPDATE CONTINUAT **********
--		
--		-- o_Uacc_ref<=Uacc_ref; --Only in the debug mode with ARRIA FPGA!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

--		------------------------------------------------------------------------------------------------------
--		-- END OF THE FSM 
--		------------------------------------------------------------------------------------------------------





-- ----------------------------------------------------------------		
--	---------------  COMPARATOR  -------------------------
--	----------------------------------------------------------------
		
		-- Automatic threshold selection. Non-stochastic comparator (parallel)
		GENARATE_COMPARATOR_NORMAL: for k in 0 to FINGERPRINT_ARRAY-1 generate
		    Uacc_diff_ref(k)<=Uacc_ref1(k)-Uacc_ref2(k) when Uacc_ref1(k)>Uacc_ref2(k) else Uacc_ref2(k)-Uacc_ref1(k);
			 Uacc_ref_normal(k)<=Uacc_ref(k)(2*BIT_PRECISION-1)&Uacc_ref(k)(BIT_PRECISION-2 DOWNTO 0);
    		 Udiff(k)<= Uacc_ref_normal(k)-fingerprint_vector(k) when Uacc_ref_normal(k)>fingerprint_vector(k) else fingerprint_vector(k)-Uacc_ref_normal(k);
--    		 Uacc_delta(k)<=shift_left(Uacc_diff_ref(k),1)-shift_right(Uacc_diff_ref(k),2);--Threshold=1.75Delta
--    		 Uacc_delta(k)<=shift_right(Uacc_diff_ref(k),1)+Uacc_diff_ref(k); --Threshold=1.5*Delta
   -- 		 Sdiff(k)<= '1' when shift_left(Uacc_diff_ref(k),1)>Udiff(k) else '0'; --Threshold=2*Delta

--		with i_exp_delta select
--					Uacc_delta(k)<=shift_left(Uacc_diff_ref(k),0) when 0,
--										shift_left(Uacc_diff_ref(k),1) when 1,
--										shift_left(Uacc_diff_ref(k),2) when 2,
--										shift_left(Uacc_diff_ref(k),3) when others;
--		Uacc_delta(k)<=Uacc_diff_ref(k)+shift_left(Uacc_diff_ref(k),1);
		Uacc_delta(k)<=shift_right(Uacc_diff_ref(k),1);
		
    		 Sdiff(k)<= '1' when Uacc_delta(k)>Udiff(k) else '0'; -- Threshold=Delta                                                                                                                                                                         (k)>Udiff(k) else '0'; -- Threshold=Delta
		end generate GENARATE_COMPARATOR_NORMAL;
		Sref<=(others=>'1');
      y_out<='1' when Sdiff=Sref else '0';
		 
--        y_out<='1' when Sdiff=X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF" else '0';
--    
--        --Sequential implementation of the comparator
--       
----		mac_inst : entity work.signed_compare_accumulate_sincron
----		port map (clk, internal_n_rst,  
----				  Uacc_diff_ref(index_neuron_aux), --weights 
----				  Udiff(index_neuron_aux), -- State vector z=(u,x)
----				  '1', --Bias of MAC function
----				  y_out -- Adder output
----		);
--
--		-- Stochastic Comparators: Activats en les versions anteriors al 06/03/2021
--		--LFSR
--		
----		lfsr_inst : entity work.lfsr_8
----		port map (clk,ctrl_n_rst,'1',X"AB",lfsr);
--		
----		GENERATE_COMPARATOR: for k in 0 to FINGERPRINT_ARRAY-1 generate
----			Uacc_ref_normal(k)<=Uacc_ref(k)(2*BIT_PRECISION-1)&Uacc_ref(k)(BIT_PRECISION-2 DOWNTO 0);
----			Udiff(k)<= Uacc_ref_normal(k)-fingerprint_vector(k) when Uacc_ref_normal(k)>fingerprint_vector(k) else fingerprint_vector(k)-Uacc_ref_normal(k);
----			Uoverf(k)<=unsigned(Udiff(k)(BIT_PRECISION-1 DOWNTO PRECISION_COMPARE+1));
----			Udiff_u(k)<=unsigned(Udiff(k)(PRECISION_COMPARE DOWNTO PRECISION_COMPARE-7)); --8 bit selection for the input of the reservoir
----			Sdiff(k)<='1' when (Udiff_u(k)>unsigned(lfsr)) else '0'; --Conversion to stochastic signal
----		end generate GENERATE_COMPARATOR;
----        GENERATE_OVERFLOW: for k in 0 to FINGERPRINT_ARRAY-1 generate
----            overf(k)<='1' when Uoverf(k)>to_unsigned(0,Uoverf(k)'length) else '0';
----        end generate GENERATE_OVERFLOW;
----        y_out_overf<= '1' when overf>to_unsigned(0,overf'length) else '0';
----		GENERATE_COMMPARATORS: for l in 0 to 7 generate
----		    max_diff(l)<='0' when Sdiff( 8*l TO (8*(l+1)-1))=x"00" else '1'; -- ojo, aqui hi pot haver una possible errada que m'haruia passat per alt.
------		    max_diff(l)<='0' when to_integer(unsigned(Sdiff(8*l to 8*(l+1)-1)))=0 else '1';
----		    p2b_converter_inst : entity WORK.p2b_8b
----		    port map (clk,ctrl_n_rst,'1',max_diff(l),X"FF",similarity(l));
----		    y_out_sim(l)<='0' when to_integer(unsigned(similarity(l)))>(THRESHOLD_VECTOR(l)+THRESHOLD) else '1';
----	       	y_out_multith(l)<= y_out_sim(l) AND NOT (y_out_overf);
----		end generate GENERATE_COMMPARATORS;
----        y_out<='1' when y_out_multith=X"FF" else '0';
--
---- Fins aqui la versio estocastica multi-threshold, operativa fins 06/03/2021
        
   	y_out_long<='0' when outstate=non_detection else '1'; -- only in the PETRA DEV. PHASE
		o_detect<=y_out_long; -- QUITARLO EN EL MODO NORMAL DE OPERACION !!!!-- only in the PETRA DEV. PHASE
		--
		process (clk)
		begin
			if (rising_edge(clk)) then
				if ctrl_n_rst = '0' then
					outstate <= non_detection;
				else
					case outstate is
						when non_detection=>
							if internal_enable='1' and y_count='1' then
								if y_out='1' then
									outstate<=detection;
								end if;
							end if;
						when detection=>
							if internal_enable='1' and y_count='1' then
								if y_out='0' then
									outstate<=non_detection;
								end if;
							end if;
					end case;
				end if;
			end if;

		end process;

		
	-- End of Stochastic Comparator ----------------------------------------------------------------------------

	-------------------------------------------------------------------
	-- Internal Reservoir Weights -------------------------------------------------------------------
	-------------------------------------------------------------------
	bias<=to_signed(-27108,2*BIT_PRECISION);
	weights(0)<=to_signed(24,BIT_PRECISION);
	weights(1)<=to_signed(21,BIT_PRECISION);
	weights(2)<=to_signed(36,BIT_PRECISION);
	weights(3)<=to_signed(-140,BIT_PRECISION);
	weights(4)<=to_signed(-146,BIT_PRECISION);
	weights(5)<=to_signed(-114,BIT_PRECISION);
	weights(6)<=to_signed(-17,BIT_PRECISION);
	weights(7)<=to_signed(209,BIT_PRECISION);
	weights(8)<=to_signed(232,BIT_PRECISION);
	weights(9)<=to_signed(-8,BIT_PRECISION);
	weights(10)<=to_signed(-36,BIT_PRECISION);
	weights(11)<=to_signed(-149,BIT_PRECISION);
	weights(12)<=to_signed(347,BIT_PRECISION);
	weights(13)<=to_signed(99,BIT_PRECISION);
	weights(14)<=to_signed(-502,BIT_PRECISION);
	weights(15)<=to_signed(-156,BIT_PRECISION);
	weights(16)<=to_signed(538,BIT_PRECISION);
	weights(17)<=to_signed(-263,BIT_PRECISION);
	weights(18)<=to_signed(15,BIT_PRECISION);
	weights(19)<=to_signed(241,BIT_PRECISION);
	weights(20)<=to_signed(22,BIT_PRECISION);
	weights(21)<=to_signed(-241,BIT_PRECISION);
	weights(22)<=to_signed(154,BIT_PRECISION);
	weights(23)<=to_signed(-302,BIT_PRECISION);
	weights(24)<=to_signed(-67,BIT_PRECISION);
	weights(25)<=to_signed(130,BIT_PRECISION);
	weights(26)<=to_signed(136,BIT_PRECISION);
	weights(27)<=to_signed(131,BIT_PRECISION);
	weights(28)<=to_signed(-15,BIT_PRECISION);
	weights(29)<=to_signed(278,BIT_PRECISION);
	weights(30)<=to_signed(521,BIT_PRECISION);
	weights(31)<=to_signed(131,BIT_PRECISION);
	weights(32)<=to_signed(83,BIT_PRECISION);
	weights(33)<=to_signed(155,BIT_PRECISION);
	weights(34)<=to_signed(154,BIT_PRECISION);
	weights(35)<=to_signed(106,BIT_PRECISION);
	weights(36)<=to_signed(23,BIT_PRECISION);
	weights(37)<=to_signed(-83,BIT_PRECISION);
	weights(38)<=to_signed(39,BIT_PRECISION);
	weights(39)<=to_signed(-88,BIT_PRECISION);
	weights(40)<=to_signed(-20,BIT_PRECISION);
	weights(41)<=to_signed(-88,BIT_PRECISION);
	weights(42)<=to_signed(176,BIT_PRECISION);
	weights(43)<=to_signed(-202,BIT_PRECISION);
	weights(44)<=to_signed(-484,BIT_PRECISION);
	weights(45)<=to_signed(1,BIT_PRECISION);
	weights(46)<=to_signed(-68,BIT_PRECISION);
	weights(47)<=to_signed(-132,BIT_PRECISION);
	weights(48)<=to_signed(243,BIT_PRECISION);
	weights(49)<=to_signed(-232,BIT_PRECISION);
	weights(50)<=to_signed(9,BIT_PRECISION);
	weights(51)<=to_signed(145,BIT_PRECISION);
	weights(52)<=to_signed(-500,BIT_PRECISION);
	weights(53)<=to_signed(181,BIT_PRECISION);
	weights(54)<=to_signed(432,BIT_PRECISION);
	weights(55)<=to_signed(40,BIT_PRECISION);
	weights(56)<=to_signed(-257,BIT_PRECISION);
	weights(57)<=to_signed(-109,BIT_PRECISION);
	weights(58)<=to_signed(-50,BIT_PRECISION);
	weights(59)<=to_signed(-121,BIT_PRECISION);
	weights(60)<=to_signed(-232,BIT_PRECISION);
	weights(61)<=to_signed(-78,BIT_PRECISION);
	weights(62)<=to_signed(-173,BIT_PRECISION);
	weights(63)<=to_signed(16,BIT_PRECISION);
	weights(64)<=to_signed(-249,BIT_PRECISION);
	weights(65)<=to_signed(96,BIT_PRECISION);
	weights(66)<=to_signed(-37,BIT_PRECISION);
	weights(67)<=to_signed(-245,BIT_PRECISION);
	weights(68)<=to_signed(36,BIT_PRECISION);
	weights(69)<=to_signed(297,BIT_PRECISION);
	weights(70)<=to_signed(-614,BIT_PRECISION);
	weights(71)<=to_signed(312,BIT_PRECISION);
	weights(72)<=to_signed(-91,BIT_PRECISION);
	weights(73)<=to_signed(255,BIT_PRECISION);
	weights(74)<=to_signed(-168,BIT_PRECISION);
	weights(75)<=to_signed(113,BIT_PRECISION);
	weights(76)<=to_signed(-355,BIT_PRECISION);
	weights(77)<=to_signed(305,BIT_PRECISION);
	weights(78)<=to_signed(202,BIT_PRECISION);
	weights(79)<=to_signed(146,BIT_PRECISION);
	weights(80)<=to_signed(-120,BIT_PRECISION);
	weights(81)<=to_signed(73,BIT_PRECISION);
	weights(82)<=to_signed(-393,BIT_PRECISION);
	weights(83)<=to_signed(-72,BIT_PRECISION);
	weights(84)<=to_signed(308,BIT_PRECISION);
	weights(85)<=to_signed(21,BIT_PRECISION);
	weights(86)<=to_signed(224,BIT_PRECISION);
	weights(87)<=to_signed(-219,BIT_PRECISION);
	weights(88)<=to_signed(-253,BIT_PRECISION);
	weights(89)<=to_signed(82,BIT_PRECISION);
	weights(90)<=to_signed(320,BIT_PRECISION);
	weights(91)<=to_signed(-378,BIT_PRECISION);
	weights(92)<=to_signed(-238,BIT_PRECISION);
	weights(93)<=to_signed(-58,BIT_PRECISION);
	weights(94)<=to_signed(138,BIT_PRECISION);
	weights(95)<=to_signed(69,BIT_PRECISION);
	weights(96)<=to_signed(-159,BIT_PRECISION);
	weights(97)<=to_signed(393,BIT_PRECISION);
	weights(98)<=to_signed(-119,BIT_PRECISION);
	weights(99)<=to_signed(-328,BIT_PRECISION);
	weights(100)<=to_signed(435,BIT_PRECISION);
	weights(101)<=to_signed(105,BIT_PRECISION);
	weights(102)<=to_signed(-214,BIT_PRECISION);
	weights(103)<=to_signed(22,BIT_PRECISION);
	weights(104)<=to_signed(-236,BIT_PRECISION);
	weights(105)<=to_signed(96,BIT_PRECISION);
	weights(106)<=to_signed(-56,BIT_PRECISION);
	weights(107)<=to_signed(55,BIT_PRECISION);
	weights(108)<=to_signed(-232,BIT_PRECISION);
	weights(109)<=to_signed(89,BIT_PRECISION);
	weights(110)<=to_signed(404,BIT_PRECISION);
	weights(111)<=to_signed(-196,BIT_PRECISION);
	weights(112)<=to_signed(83,BIT_PRECISION);
	weights(113)<=to_signed(430,BIT_PRECISION);
	weights(114)<=to_signed(-432,BIT_PRECISION);
	weights(115)<=to_signed(245,BIT_PRECISION);
	weights(116)<=to_signed(-315,BIT_PRECISION);
	weights(117)<=to_signed(344,BIT_PRECISION);
	weights(118)<=to_signed(-146,BIT_PRECISION);
	weights(119)<=to_signed(-76,BIT_PRECISION);
	weights(120)<=to_signed(289,BIT_PRECISION);
	weights(121)<=to_signed(-229,BIT_PRECISION);
	weights(122)<=to_signed(70,BIT_PRECISION);
	weights(123)<=to_signed(-34,BIT_PRECISION);
	weights(124)<=to_signed(201,BIT_PRECISION);
	weights(125)<=to_signed(-304,BIT_PRECISION);
	weights(126)<=to_signed(141,BIT_PRECISION);
	weights(127)<=to_signed(219,BIT_PRECISION);
	weights(128)<=to_signed(-331,BIT_PRECISION);
	weights(129)<=to_signed(-186,BIT_PRECISION);
	weights(130)<=to_signed(-230,BIT_PRECISION);
	weights(131)<=to_signed(387,BIT_PRECISION);
	weights(132)<=to_signed(-327,BIT_PRECISION);
	weights(133)<=to_signed(-28,BIT_PRECISION);
	weights(134)<=to_signed(-156,BIT_PRECISION);
	weights(135)<=to_signed(-88,BIT_PRECISION);
	weights(136)<=to_signed(-500,BIT_PRECISION);
	weights(137)<=to_signed(142,BIT_PRECISION);
	weights(138)<=to_signed(-144,BIT_PRECISION);
	weights(139)<=to_signed(421,BIT_PRECISION);
	weights(140)<=to_signed(151,BIT_PRECISION);
	weights(141)<=to_signed(-483,BIT_PRECISION);
	weights(142)<=to_signed(0,BIT_PRECISION);
	weights(143)<=to_signed(43,BIT_PRECISION);
	weights(144)<=to_signed(233,BIT_PRECISION);
	weights(145)<=to_signed(-160,BIT_PRECISION);
	weights(146)<=to_signed(-122,BIT_PRECISION);
	weights(147)<=to_signed(-262,BIT_PRECISION);
	weights(148)<=to_signed(399,BIT_PRECISION);
	weights(149)<=to_signed(-184,BIT_PRECISION);
	weights(150)<=to_signed(-306,BIT_PRECISION);
	weights(151)<=to_signed(217,BIT_PRECISION);
	weights(152)<=to_signed(126,BIT_PRECISION);
	weights(153)<=to_signed(-90,BIT_PRECISION);
	weights(154)<=to_signed(-168,BIT_PRECISION);
	weights(155)<=to_signed(156,BIT_PRECISION);
	weights(156)<=to_signed(38,BIT_PRECISION);
	weights(157)<=to_signed(-51,BIT_PRECISION);
	weights(158)<=to_signed(-62,BIT_PRECISION);
	weights(159)<=to_signed(57,BIT_PRECISION);
	weights(160)<=to_signed(391,BIT_PRECISION);
	weights(161)<=to_signed(192,BIT_PRECISION);
	weights(162)<=to_signed(-108,BIT_PRECISION);
	weights(163)<=to_signed(40,BIT_PRECISION);
	weights(164)<=to_signed(-201,BIT_PRECISION);
	weights(165)<=to_signed(164,BIT_PRECISION);
	weights(166)<=to_signed(264,BIT_PRECISION);
	weights(167)<=to_signed(28,BIT_PRECISION);
	weights(168)<=to_signed(-66,BIT_PRECISION);
	weights(169)<=to_signed(179,BIT_PRECISION);
	weights(170)<=to_signed(-44,BIT_PRECISION);
	weights(171)<=to_signed(484,BIT_PRECISION);
	weights(172)<=to_signed(-448,BIT_PRECISION);
	weights(173)<=to_signed(-176,BIT_PRECISION);
	weights(174)<=to_signed(62,BIT_PRECISION);
	weights(175)<=to_signed(-127,BIT_PRECISION);
	weights(176)<=to_signed(3,BIT_PRECISION);
	weights(177)<=to_signed(394,BIT_PRECISION);
	weights(178)<=to_signed(-295,BIT_PRECISION);
	weights(179)<=to_signed(-526,BIT_PRECISION);
	weights(180)<=to_signed(400,BIT_PRECISION);
	weights(181)<=to_signed(-58,BIT_PRECISION);
	weights(182)<=to_signed(-105,BIT_PRECISION);
	weights(183)<=to_signed(-340,BIT_PRECISION);
	weights(184)<=to_signed(402,BIT_PRECISION);
	weights(185)<=to_signed(-255,BIT_PRECISION);
	weights(186)<=to_signed(199,BIT_PRECISION);
	weights(187)<=to_signed(-771,BIT_PRECISION);
	weights(188)<=to_signed(325,BIT_PRECISION);
	weights(189)<=to_signed(-443,BIT_PRECISION);
	weights(190)<=to_signed(327,BIT_PRECISION);
	weights(191)<=to_signed(-143,BIT_PRECISION);
	weights(192)<=to_signed(-456,BIT_PRECISION);
	weights(193)<=to_signed(261,BIT_PRECISION);
	weights(194)<=to_signed(-319,BIT_PRECISION);
	weights(195)<=to_signed(-182,BIT_PRECISION);
	weights(196)<=to_signed(226,BIT_PRECISION);
	weights(197)<=to_signed(-510,BIT_PRECISION);
	weights(198)<=to_signed(216,BIT_PRECISION);
	weights(199)<=to_signed(638,BIT_PRECISION);
	weights(200)<=to_signed(-20,BIT_PRECISION);
	weights(201)<=to_signed(271,BIT_PRECISION);
	weights(202)<=to_signed(-62,BIT_PRECISION);
	weights(203)<=to_signed(268,BIT_PRECISION);
	weights(204)<=to_signed(-240,BIT_PRECISION);
	weights(205)<=to_signed(180,BIT_PRECISION);
	weights(206)<=to_signed(193,BIT_PRECISION);
	weights(207)<=to_signed(-85,BIT_PRECISION);
	weights(208)<=to_signed(117,BIT_PRECISION);
	weights(209)<=to_signed(-375,BIT_PRECISION);
	weights(210)<=to_signed(170,BIT_PRECISION);
	weights(211)<=to_signed(80,BIT_PRECISION);
	weights(212)<=to_signed(211,BIT_PRECISION);
	weights(213)<=to_signed(516,BIT_PRECISION);
	weights(214)<=to_signed(-137,BIT_PRECISION);
	weights(215)<=to_signed(298,BIT_PRECISION);
	weights(216)<=to_signed(-642,BIT_PRECISION);
	weights(217)<=to_signed(87,BIT_PRECISION);
	weights(218)<=to_signed(14,BIT_PRECISION);
	weights(219)<=to_signed(319,BIT_PRECISION);
	weights(220)<=to_signed(-42,BIT_PRECISION);
	weights(221)<=to_signed(-54,BIT_PRECISION);
	weights(222)<=to_signed(-90,BIT_PRECISION);
	weights(223)<=to_signed(191,BIT_PRECISION);
	weights(224)<=to_signed(30,BIT_PRECISION);
	weights(225)<=to_signed(178,BIT_PRECISION);
	weights(226)<=to_signed(-394,BIT_PRECISION);
	weights(227)<=to_signed(169,BIT_PRECISION);
	weights(228)<=to_signed(-15,BIT_PRECISION);
	weights(229)<=to_signed(148,BIT_PRECISION);
	weights(230)<=to_signed(-138,BIT_PRECISION);
	weights(231)<=to_signed(242,BIT_PRECISION);
	weights(232)<=to_signed(370,BIT_PRECISION);
	weights(233)<=to_signed(-215,BIT_PRECISION);
	weights(234)<=to_signed(-78,BIT_PRECISION);
	weights(235)<=to_signed(401,BIT_PRECISION);
	weights(236)<=to_signed(55,BIT_PRECISION);
	weights(237)<=to_signed(-80,BIT_PRECISION);
	weights(238)<=to_signed(65,BIT_PRECISION);
	weights(239)<=to_signed(-215,BIT_PRECISION);
	weights(240)<=to_signed(107,BIT_PRECISION);
	weights(241)<=to_signed(-42,BIT_PRECISION);
	weights(242)<=to_signed(364,BIT_PRECISION);
	weights(243)<=to_signed(-19,BIT_PRECISION);
	weights(244)<=to_signed(-273,BIT_PRECISION);
	weights(245)<=to_signed(-259,BIT_PRECISION);
	weights(246)<=to_signed(366,BIT_PRECISION);
	weights(247)<=to_signed(24,BIT_PRECISION);
	weights(248)<=to_signed(94,BIT_PRECISION);
	weights(249)<=to_signed(-117,BIT_PRECISION);
	weights(250)<=to_signed(158,BIT_PRECISION);
	weights(251)<=to_signed(21,BIT_PRECISION);
	weights(252)<=to_signed(-299,BIT_PRECISION);
	weights(253)<=to_signed(-16,BIT_PRECISION);
	weights(254)<=to_signed(-312,BIT_PRECISION);
	weights(255)<=to_signed(133,BIT_PRECISION);
	
	
END principal;